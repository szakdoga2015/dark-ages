<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.04.01.
 * Time: 17:22
 */
require_once "../controls/mysqlkapcs.php";


class jatekos
{

    public $nyersanyag;
    public $id;
    public $kapcsolat;


    public function __construct($id){
        $this->id=$id;
        $this->kapcsolat=new mysqlkapcs();
        $this->nyersanyag=  $this->kapcsolat->jatekosNyersanyag($this->id);

    }

    public function epuletek(){
       return $this->kapcsolat->jatekosEpuletek($this->id);
    }

    public function egysegek(){
        $stmt=$this->kapcsolat->dbc->prepare("SELECT * FROM Dark_Ages.jatekos_has_egysegek WHERE jatekos_id=?");
        $stmt->execute(array($this->id));
        $egysegek=$stmt->fetch(PDO::FETCH_ASSOC);
        return $egysegek;

    }

    public function frisitnyers(){

        $this->nyersanyag=$this->kapcsolat->jatekosNyersanyag($this->id);
    }

    public function tostring(){
        var_dump(array_merge($this->nyersanyag,$this->epuletek));
    }

    public function poz(){
        $dbc=new mysqlkapcs();

        $stmt=$dbc->dbc->prepare("SELECT poz_x, poz_y FROM Dark_Ages.jatekos WHERE id=?");
        $stmt->execute(array($this->id));
        return $stmt->fetch(PDO::FETCH_ASSOC);

    }
    /*
    public function egysegek(){
        $dbc=new mysqlkapcs();

        $stmt=$dbc->dbc->prepare("SELECT * FROM Dark_Ages.jatekos_has_egysegek WHERE jatekos_id=?");
        $stmt->execute(array($this->id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }*/

}




