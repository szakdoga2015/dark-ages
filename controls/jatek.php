<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.02.11.
 * Time: 17:55
 */
require_once "../models/jatekos.php";
require_once "epulet_control.php";
class jatek
{

    private $jatekos;

    public function __construct($id)
    {
        $this->jatekos = new jatekos($id);
    }

    /**
     * visza adja a jatekos tulajdonában lévő épületeket
     * @return array
     */
    public function getepuletek()
    {
        return $this->jatekos->epuletek();
    }

    /** vissza adja a nyersanayg táblában lévő mezőket (aktnyersanayag,termelés,hangulat,kutatásipont)
     * @return mixed
     */
    public function getnyersanyag()
    {
        return $this->jatekos->nyersanyag;
    }

    /**
     * ujra kiolvassa  a jatekos nyersanyagat az adatbázisból (oránként kéne meg hívni)
     */
    public function frisitnyers()
    {
        $this->jatekos->frisitnyers();
    }

    /**
     * Felépíti a kövtkezö szintett az adot épületből ha teljesülnek a feltételek
     * ha nem hiba üzenetett ad vissza
     * @param $epuletnev
     */
    public function kovszintepit($epuletnev){
        $epvontrol= new epulet_control($this->jatekos->id,$this->jatekos->epuletek());
        $this->frisitnyers();
        $seg=$epvontrol->kovetkezoszintepit($epuletnev,$this->jatekos->nyersanyag);
        $this->frisitnyers();
        return $seg;
    }

    /**
     * @param $epuletnev
     * @return tömb a következő szint adataival
     */
    public function kovnyers ($epuletnev){
        $epvontrol= new epulet_control($this->jatekos->id,$this->jatekos->epuletek());
        return $epvontrol->kovetkezoszintkoltseg($epuletnev);
    }

    /**
     * megadja hogy a játékosnak meg van e az épülete;
     */
    public function epuletkesz(){
        $epvontrol= new epulet_control($this->jatekos->id,$this->jatekos->epuletek());
        $epvontrol->epuletkesz();
    }

}