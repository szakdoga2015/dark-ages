<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.04.23.
 * Time: 19:37
 */
require_once "mysqlkapcs.php";
require_once "../models/jatekos.php";

class egyseg_gyartas {

      public $egysegek;
      private $jatekos;

    public function __construct($id){
        $dbc=new mysqlkapcs();
        $stmt= $dbc->dbc->prepare("SELECT * FROM Dark_Ages.egysegek");
        $stmt->execute();
        $this->egysegek=$stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->jatekos=new jatekos($id);
    }

    public function gyarthatoegysegek_piac(){

        $dbc=new mysqlkapcs();
        $stmt=$dbc->dbc->prepare("SELECT `max_kereskedo`,`max_szeker` from Dark_Ages.piac JOIN Dark_Ages.jatekos on Dark_Ages.piac.azon=Dark_Ages.jatekos.piac_azon where Dark_Ages.jatekos.id=?");
        $stmt->execute(array($this->jatekos->id));
        $egysegmax=$stmt->fetch(PDO::FETCH_ASSOC);

        $stmt=$dbc->dbc->prepare("SELECT kereskedo,szeker from Dark_Ages.jatekos_has_egysegek WHERE jatekos_id=?;");
        $stmt->execute(array($this->jatekos->id));
        $jatekosegysegek=$stmt->fetch(PDO::FETCH_ASSOC);

        $f['epitheto']=($egysegmax['max_kereskedo']-$jatekosegysegek['kereskedo']);

        $valasz['kereskedo']=(array_merge($f,$this->egysegek[3]));

        $f['epitheto']=($egysegmax['max_szeker']-$jatekosegysegek['szeker']);

        $valasz['szeker']=(array_merge($f,$this->egysegek[4]));
        return $valasz;

    }

    public function gyarthatoegyseg_laktanya(){

        $dbc=new mysqlkapcs();
        $stmt=$dbc->dbc->prepare("SELECT `max_hadsereg` from Dark_Ages.laktanya JOIN Dark_Ages.jatekos on Dark_Ages.laktanya.azon=Dark_Ages.jatekos.laktanya_azon where Dark_Ages.jatekos.id=?");
        $stmt->execute(array($this->jatekos->id));
        $egysegmax=$stmt->fetch(PDO::FETCH_ASSOC);

        $stmt=$dbc->dbc->prepare("SELECT kardos+landzsas+ijjasz from Dark_Ages.jatekos_has_egysegek WHERE jatekos_id=?;");
        $stmt->execute(array($this->jatekos->id));
        $jatekosegysegek=$stmt->fetch(PDO::FETCH_ASSOC);

        $f["epitheto"]=$egysegmax['max_hadsereg']-$jatekosegysegek['kardos+landzsas+ijjasz'];

        $valasz['kardos']=(array_merge($f,$this->egysegek[0]));
        $valasz['landzsas']=(array_merge($f,$this->egysegek[1]));
        $valasz['ijjasz']=(array_merge($f,$this->egysegek[2]));

        return $valasz;

    }

    /**
     * @param $egysegek aszoc tömb szeker->db,kereskedo->db
     * @return string
     */
    public function egyseggyartas_piac($egysegek){
        $dbc=new mysqlkapcs();
        $fa=($egysegek['kereskedo']*$this->egysegek[3]['fa'])+$egysegek['szeker']*$this->egysegek[4]['fa'];
        $buza=($egysegek['kereskedo']*$this->egysegek[3]['buza'])+$egysegek['szeker']*$this->egysegek[4]['buza'];

        if($this->jatekos->nyersanyag['fa']>=$fa && $this->jatekos->nyersanyag['buza']>=$buza){
            $stmt=$dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=?, szeker=? where jatekos_id=?");
            $stmt->execute(array($egysegek['kereskedo'],$egysegek['szeker'],$this->jatekos->id));

            $stmt=$dbc->dbc->prepare("UPDATE  Dark_Ages.jatekos_nyersanyag SET `fa`=?,`buza`=? WHERE jatekos_id=?");
            $stmt->execute(array($this->jatekos->nyersanyag['fa']-$fa, $this->jatekos->nyersanyag['buza']-$buza,$this->jatekos->id));


        }else{
            return "Nincs elég nyersanyagod";
        }
    }


    public function egysegygartas_laktanya($egysegek){
        $dbc=new mysqlkapcs();
        $kard=($egysegek['kardos']*$this->egysegek[0]['koltseg_kard'])+($egysegek['landzsas']*$this->egysegek[0]['koltseg_kard'])+($egysegek['ijjasz']*$this->egysegek[0]['koltseg_kard']);
        $ij=($egysegek['kardos']*$this->egysegek[0]['koltseg_ij'])+($egysegek['landzsas']*$this->egysegek[0]['koltseg_ij'])+($egysegek['ijjasz']*$this->egysegek[0]['koltseg_ij']);
        $landzsa=($egysegek['kardos']*$this->egysegek[0]['koltseg_landzsa'])+($egysegek['landzsas']*$this->egysegek[0]['koltseg_landzsa'])+($egysegek['ijjasz']*$this->egysegek[0]['koltseg_landzsa']);

        if($this->jatekos->nyersanyag['kard']>=$kard && $this->jatekos->nyersanyag['ij']>=$ij &&$this->jatekos->nyersanyag['landzsa']>=$landzsa){
            $stmt=$dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kardos=?,landzsas=?,ijjasz=? WHERE jatekos_id=?");
            $stmt->execute(array($egysegek['kardos'],$egysegek['landzsas'],$egysegek['ijjasz'],$this->jatekos->id,));

            $stmt=$dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_nyersanyag SET `kard`=?, `ij`=?, `landzsa`=? WHERE jatekso_id=?");
            $stmt->execute(array($this->jatekos->nyersanyag['kard']-$kard, $this->jatekos->nyersanyag['ij']-$ij,$this->jatekos->nyersanyag['landzsa']-$landzsa,$this->jatekos->id));


        }else{
            return "Nincs elég nyersanyagod";
        }
    }

    public function egysegek(){
        $pdo=new mysqlkapcs();
        $stmt=$pdo->dbc->prepare("SELECT * FROM Dark_Ages.jatekos_has_egysegek WHERE jatekos_id=?");
        $stmt->execute(array($this->jatekos->id));
        $egysegek=$stmt->fetch(PDO::FETCH_ASSOC);
        return $egysegek;
    }

}
