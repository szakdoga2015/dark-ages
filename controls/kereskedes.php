<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.04.13.
 * Time: 18:28
 */

require_once "mysqlkapcs.php";
require_once "../models/jatekos.php";

class kereskedes {

    private $dbc;
    private $jatekos;


    public function __construct($id){
        $this->dbc=new mysqlkapcs();
        $this->jatekos=new jatekos($id);
    }

    public function nyiltajanlat(){
        $stmt=$this->dbc->dbc->prepare("SELECT * FROM Dark_Ages.kereskedes_ajanlat WHERE jatekos_id!=? ORDER BY meghirdetisdatum;");
        $stmt->execute(array($this->jatekos->id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function keres($mit){

        $stmt=$this->dbc->dbc->prepare("SELECT * FROM Dark_Ages.kereskedes_ajanlat
                                        WHERE jatekos_id!=? &&  ".$mit."_keres>0");

        $stmt->execute(array($this->jatekos->id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
         $stmt->queryString;
    }

    public function kinal($mit){
        $stmt=$this->dbc->dbc->prepare("SELECT * FROM Dark_Ages.kereskedes_ajanlat
                                        WHERE jatekos_id!=? &&  ".$mit."_ajanl>0");
        $stmt->execute(array($this->jatekos->id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $ajanlat tömb nyersertekek sorend:buza,fa,vas,ko
     * @param $keres tömb nyersertekek sorend:buza,fa,vas,ko
     * @param $ajanlat tömb nyersertekek sorend:buza,fa,vas,ko
     * @param $keres tömb nyersertekek sorend:buza,fa,vas,ko
     * @param $jatekid
     * @param $jatekospoz
     */
    public function ajanlattesz($ajanlat,$keres){

        $osznyers=$ajanlat['buza']+$ajanlat['fa']+$ajanlat['vas']+$ajanlat['ko'];
        $szekerdb=ceil($osznyers/500.0);


        if($this->jatekos->nyersanyag['fa']>=$ajanlat['fa'] && $this->jatekos->nyersanyag['ko']>=$ajanlat['ko'] &&
           $this->jatekos->nyersanyag['vas']>=$ajanlat['vas'] && $this->jatekos->nyersanyag['buza']>=$ajanlat['buza']&&
            $szekerdb<=$this->jatekos->egysegek()['szeker'] && $this->jatekos->egysegek()["kereskedo"]>=1){

            $nyers['fa']=$this->jatekos->nyersanyag['fa']-$ajanlat['fa'];
            $nyers['ko']=$this->jatekos->nyersanyag['ko']-$ajanlat['ko'];
            $nyers['vas']=$this->jatekos->nyersanyag['vas']-$ajanlat['vas'];
            $nyers['buza']=$this->jatekos->nyersanyag['buza']-$ajanlat['buza'];

            $this->dbc->jatekosnyersbeal($this->jatekos->id,$nyers);
            $this->jatekos->frisitnyers();

            $poz= $this->jatekos->poz();
            $stmt = $this->dbc->dbc->prepare("INSERT INTO Dark_Ages.kereskedes_ajanlat(id,`buza_ajanl`,`fa_ajanl`,`vas_ajanl`,`ko_ajanl`,
                                                                                `buza_keres`,`fa_keres`,`vas_keres`,`ko_keres`,
                                                                                `jatekos_id`,`pozx`,`pozy`,szeker)
                                                                         VALUES (uuid(),?,?,?,?,
                                                                                 ?,?,?,?,
                                                                                 ?,?,?,?)");
            $stmt->execute(array($ajanlat['buza'],$ajanlat['fa'],$ajanlat['vas'],$ajanlat['ko'],
                                $keres['buza'],$keres['fa'],$keres['vas'],$keres['ko'],
                                ($this->jatekos->id),$poz['poz_x'],$poz['poz_y'],$szekerdb));

            $stmt= $this->dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=kereskedo-1, szeker=szeker-? where jatekos_id=?");
            $stmt->execute(array($szekerdb, $this->jatekos->id));

        }else{
            return "Nincs elegendő nyersanyagot vagy egységed ehhez az ajánlathoz! ";
        }
    }

    /**
     * @param $azohirdet
     * @return mixed vissza tér az kereskedés vég idő pontjával nem tudom kell e valamire
     */
    public function ajanlatelfogad($azohirdet ){
        if( $azohirdet != 0 )
        {
            $stmt=$this->dbc->dbc->prepare("SELECT * FROM Dark_Ages.kereskedes_ajanlat WHERE id=?");
            $stmt->execute(array($azohirdet));
            $hirdetes=$stmt->fetch(PDO::FETCH_ASSOC);
            print_r($hirdetes);

            if($this->jatekos->nyersanyag['fa']>=$hirdetes['fa_keres'] && $this->jatekos->nyersanyag['buza']>=$hirdetes['buza_keres'] &&
                $this->jatekos->nyersanyag['vas']>=$hirdetes['vas_keres'] && $this->jatekos->nyersanyag['ko']>=$hirdetes['ko_keres'] &&
                $this->jatekos->egysegek()['szeker']>=$hirdetes['szeker'] && $this->jatekos->egysegek()["kereskedo"]>=1){

                $nyers['fa']=$this->jatekos->nyersanyag['fa']-$hirdetes['fa_keres'];
                $nyers['ko']=$this->jatekos->nyersanyag['ko']-$hirdetes['ko_keres'];
                $nyers['vas']=$this->jatekos->nyersanyag['vas']-$hirdetes['vas_keres'];
                $nyers['buza']=$this->jatekos->nyersanyag['buza']-$hirdetes['buza_keres'];

                $this->dbc->jatekosnyersbeal($this->jatekos->id,$nyers);
                $this->jatekos->frisitnyers();

                $stmt= $this->dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=kereskedo-1, szeker=szeker-? where jatekos_id=?");
                $stmt->execute(array($hirdetes['szeker'], $this->jatekos->id));

                $kinaljatekos=new jatekos($hirdetes[jatekos_id]);

                $erkezes=erkezes($kinaljatekos);

                $stmt=$this->dbc->dbc->prepare("insert into Dark_Ages.kereskedes_folyamatban
                                      (`buza_ajanl`, `fa_ajanl`, `vas_ajanl`, `ko_ajanl`,
                                        `buza_keres`, `fa_keres`, `vas_keres`, `ko_keres`,
                                         `jatekos_id_keres`, `jatekos_id_kinal`, `erkezesiido`, `szeker`)
                                         VALUES (?,?,?,?,
                                                 ?,?,?,?
                                                 ?,?,?,?)");

                $stmt->execute(array($hirdetes['buza_ajanl'],$hirdetes['fa_ajanl'],$hirdetes['vas_ajanl'],$hirdetes['ko_ajanl'],
                    $hirdetes['buza_keres'],$hirdetes['fa_keres'],$hirdetes['vas_keres'],$hirdetes['ko_keres'],
                    $this->jatekos->id,$hirdetes['jatekos_id'],$erkezes,$hirdetes['szeker']
                ));

                $stmt=$this->dbc->dbc->prepare("DELETE FROM `kereskedes_ajanlat` WHERE id=?");
                $stmt->execute(array($hirdetes['id']));
                return $erkezes;

            }

        }


    }

    public function erkezes($jatekos){

        $anegyzet=pow($this->jatekos->poz()->poz['poz_x']-$jatekos->poz['poz_x'],2);
        $bnegyzet=pow($this->jatekos->poz()->poz['poz_y']-$jatekos->poz['poz_y'],2);

        $c=sqrt($anegyzet+$bnegyzet);
        $c=ceil($c);

        $timestamp=60*60*$c;
        $datetime=new DateTime();
        $datetime->setTimestamp($datetime->getTimestamp()+$timestamp);

        return $datetime->format('Y:m:d H:i:s');

    }

    /**
     * ha vége a kereskedésnek ezt kezeli
     */
    public  function kereskedesveg(){

        $stmt=$this->dbc->dbc->prepare("SELECT * FROM Dark_Ages.kereskedes_folyamatban WHERE erkezesiido<=? && (jatekos_id_keres=? || jatekos_id_kinal=?)");
        $stmt->execute(array(new  DateTime(),$this->jatekos->id,$this->jatekos->id));

        $keres=$stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($keres as $ertek){
            if($ertek["jatekos_id_keres"]==$this->jatekos->id){

                $nyers['fa']=$this->jatekos->nyersanyag['fa']+$ertek['fa_ajanl'];
                $nyers['ko']=$this->jatekos->nyersanyag['ko']+$ertek['ko_ajanl'];
                $nyers['vas']=$this->jatekos->nyersanyag['vas']+$ertek['vas_ajanl'];
                $nyers['buza']=$this->jatekos->nyersanyag['buza']+$ertek['buza_ajanl'];
                $this->dbc->jatekosnyersbeal($this->jatekos->id,$nyers);

                $jatekos2=new jatekos($ertek["jatekos_id_kinal"]);

                $nyers['fa']=$jatekos2->nyersanyag['fa']+$ertek['fa_keres'];
                $nyers['ko']=$jatekos2->nyersanyag['ko']+$ertek['ko_keres'];
                $nyers['vas']=$jatekos2->nyersanyag['vas']+$ertek['vas_keres'];
                $nyers['buza']=$jatekos2->nyersanyag['buza']+$ertek['buza_keres'];
                $this->dbc->jatekosnyersbeal($jatekos2->id,$nyers);

                $stmt= $this->dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=kereskedo+1, szeker=szeker+? where jatekos_id=?");
                $stmt->execute(array($ertek['szeker'], $this->jatekos->id));
                $stmt= $this->dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=kereskedo+1, szeker=szeker+? where jatekos_id=?");
                $stmt->execute(array($ertek['szeker'], $jatekos2->id));

            }else{

                $nyers['fa']=$this->jatekos->nyersanyag['fa']+$ertek['fa_ajanl'];
                $nyers['ko']=$this->jatekos->nyersanyag['ko']+$ertek['ko_ajanl'];
                $nyers['vas']=$this->jatekos->nyersanyag['vas']+$ertek['vas_ajanl'];
                $nyers['buza']=$this->jatekos->nyersanyag['buza']+$ertek['buza_ajanl'];
                $this->dbc->jatekosnyersbeal($this->jatekos->id,$nyers);

                $jatekos2=new jatekos($ertek["jatekos_id_keres"]);

                $nyers['fa']=$jatekos2->nyersanyag['fa']+$ertek['fa_keres'];
                $nyers['ko']=$jatekos2->nyersanyag['ko']+$ertek['ko_keres'];
                $nyers['vas']=$jatekos2->nyersanyag['vas']+$ertek['vas_keres'];
                $nyers['buza']=$jatekos2->nyersanyag['buza']+$ertek['buza_keres'];
                $this->dbc->jatekosnyersbeal($jatekos2->id,$nyers);

                $stmt= $this->dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=kereskedo+1, szeker=szeker+? where jatekos_id=?");
                $stmt->execute(array($ertek['szeker'], $this->jatekos->id));
                $stmt= $this->dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_has_egysegek SET kereskedo=kereskedo+1, szeker=szeker+? where jatekos_id=?");
                $stmt->execute(array($ertek['szeker'], $jatekos2->id));
            }
        }


    }

    /**
     * @return array vissza tér azöszes folyamatban lévő kereskedéssel amiben érintett
     */
    public function jatekos_folyamatbanlevokereskedesek(){
        $stmt=$this->dbc->dbc->prepare("SELECT * FROM Dark_Ages.kereskedes_folyamatban WHERE jatekos_id_keres=? || jatekos_id_kinal=?");
        $stmt->execute(array($this->jatekos->id,$this->jatekos->id));

        $keres=$stmt->fetchAll(PDO::FETCH_ASSOC);

        return $keres;
    }
}





