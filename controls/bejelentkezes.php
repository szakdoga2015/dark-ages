<?php
/**
 * Bejelentkező ellenörző
 */
require_once "mysqlkapcs.php";
require_once "helpers.php";
session_start();

function test_input1($adat)
{
    $data = trim($adat);
    $data = stripslashes($adat);
    $data = htmlspecialchars($adat);
    return $data;
}
    $nev=$jelszo="";
    $neverr=$jelszoerr="";
    $bejelentkezet=$elso=$masodik=false;


if($_SERVER["REQUEST_METHOD"]=="POST") {

    if (empty($_POST["nev"])) {
        $neverr = "Nem adtál meg nevet!";

    }else{
        $nev = test_input1($_POST["nev"]);
        $elso = true;
    }

    if (empty($_POST["jelszo"])) {
        $jelszoerr = "Nem adtál meg jelszót";

    }else{
        $jelszo = test_input1($_POST["jelszo"]);

        $masodik = true;
    }

    if ($elso && $masodik) {
        $adat = new mysqlkapcs();
        $adatok=$adat->bejelentkezes($nev);
        print_r($adatok);
        $mentettjelszo = $adatok['jelszo'];


        if (password_verify($jelszo,$mentettjelszo)) {
            $_SESSION['bejelentkezet'] = true;
            $_SESSION['id']=$adatok['id'];
            $_SESSION['nev']=$nev;
            atiranyitas("../views/jatek.php");
            $adat->epites_kesz($_SESSION['id']);
        }else{
            atiranyitas("../views/");
            echo "atiranyitva";
        }

    }
}


