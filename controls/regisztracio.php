<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.02.24.
 * Time: 16:16
 *
 * Regisztráció
 *
 */
require_once "mysqlkapcs.php";
require_once "mailer.php";
require_once "helpers.php";


function test_input($adat)
{
    $data = trim($adat);
    $data = stripslashes($adat);
    $data = htmlspecialchars($adat);
    return $data;
}


$captcha=$nev=$email=$jelszo='';
$captchaerr=$neverr=$emailerr=$jelszoerr='';

if($_SERVER["REQUEST_METHOD"]=="POST") {


    $privatekey = "6LfaCgYTAAAAAIUfRB_1gOI-WyDZ1DQY34OyK96a";
    $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $privatekey . "&response=" . $_POST['g-recaptcha-response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']), true);

    if ($response['success'] == true) {

        if (!empty($_POST['nev'])) {
            $nev = test_input($_POST['nev']);
            $adatbazis = new mysqlkapcs();
            if ($adatbazis->felhaszletezik($nev) == 0) {
                $neverr = "helyes";
            } else {
                $neverr = "A felhasználó név már foglalt";
            }
        } else {
            $neverr = "Hiányzó név!";
        }

        if (!empty($_POST['email'])) {
            $email = test_input($_POST['email']);
            $emailerr = "helyes";
        } else {
            $emailerr = "Hiányzó e-mail cím!";
        }

        if (!empty($_POST['jelszo'])) {
            $jelszo = test_input($_POST["jelszo"]);
            $jelszo = password_hash($jelszo, PASSWORD_DEFAULT);
            $jelszoerr = "helyes";
        } else {
            $jelszoerr = "Jelszó megadása kötelező";
        }

        if (strcmp("helyes", $neverr) == 0 && 0 == strcmp("helyes", $jelszoerr)
             && 0 == strcmp("helyes", $emailerr)
        ) {
            $send = new Mailer();

            $aktivalokod = $send->activation($email, $nev);
            $_SESSION['aktiv'] = $aktivalokod;
            $adatbazis->felhasznaloregisztralas($nev, $jelszo, $email, $aktivalokod);

            $adatok = $adatbazis->bejelentkezes($nev);
            $_SESSION['bejelentkezet'] = true;
            $_SESSION['id'] = $adatok['id'];
            $_SESSION['nev'] = $nev;

            atiranyitas("../views/jatek.php");
        }
    }



}
