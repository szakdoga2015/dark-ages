<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.04.30.
 * Time: 20:08
 */

require_once "mysqlkapcs.php";
require_once "../models/jatekos.php";

class szerszamgyartas {

    public $fegyver;
    private $jatekos;

    public function __construct($id){
        $dbc=new mysqlkapcs();
        $stmt= $dbc->dbc->prepare("SELECT * FROM Dark_Ages.fegyverek");
        $stmt->execute();
        $this->fegyver=$stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->jatekos=new jatekos($id);
    }

    public function gyarthatofegyver(){
        $dbc=new mysqlkapcs();
        $stmt=$dbc->dbc->prepare("SELECT `termeles_kard`,`termeles_ij`,`termeles_landzsa` from Dark_Ages.kovacs JOIN Dark_Ages.jatekos on Dark_Ages.kovacs.azon=Dark_Ages.jatekos.kovacs_azon where Dark_Ages.jatekos.id=?");
        $stmt->execute(array($this->jatekos->id));
        $gyartas=$stmt->fetch(PDO::FETCH_ASSOC);
        return $gyartas;
    }

    public function fegyvergyartas($fegyverek){
        $dbc=new mysqlkapcs();
        $fa=($fegyverek['termeles_kard']*$this->fegyver[0]['fa'])+($fegyverek['termeles_ij']*$this->fegyver[2]['fa'])+($fegyverek['termeles_landzsa']*$this->fegyver[1]['fa']);
        $vas=($fegyverek['termeles_kard']*$this->fegyver[0]['vas'])+($fegyverek['termeles_ij']*$this->fegyver[2]['vas'])+($fegyverek['termeles_landzsa']*$this->fegyver[1]['vas']);

        if($this->jatekos->nyersanyag['fa']>=$fa && $this->jatekos->nyersanyag['vas']>=$vas){
            $stmt=$dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_nyersanyag SET `kard`=kard+?, `ij`=ij+?, `landzsa`=landzsa+? WHERE jatekos_id=?");
            $stmt->execute(array($fegyverek['termeles_kard'],$fegyverek['termeles_ij'],$fegyverek['termeles_landzsa'],$this->jatekos->id));

            $stmt=$dbc->dbc->prepare("UPDATE Dark_Ages.jatekos_nyersanyag SET `vas`=?, `vas`=? WHERE jatekos_id=?");
            $stmt->execute(array($this->jatekos->nyersanyag['fa']-$fa, $this->jatekos->nyersanyag['vas']-$vas,$this->jatekos->id));

        }

    }

}


