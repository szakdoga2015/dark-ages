<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.04.20.
 * Time: 18:10
 */

class epulet_control {

    public $kapcsolat;
    public $id;
    public $epuletek;

    public function __construct($id,$epuletek){
         $this->kapcsolat=new mysqlkapcs();
        $this->id=$id;
        $this->epuletek=$epuletek;
    }

    public function kovetkezoszintkoltseg($epuletnev){

        return ($this->kapcsolat->kovetkezoszintkoltseg($this->id,$epuletnev));
    }

    public function kovetkezoszintepit($epuletnev,$nyersanyag){

        $feltelek=$this->kovetkezoszintkoltseg($epuletnev);

        $epulet_feltetel=false;

        foreach (explode(',', $feltelek['feltetel']) as $valt) {
            $feltetelepuletek = explode(' ', $valt);
            foreach ($this->epuletek as $key => $value) {

                if (($key == $feltetelepuletek[0])) {

                    if (!($value >= $feltetelepuletek[1])) {
                        return "Rendelkezned kell a(z) " . $feltetelepuletek[0] . " " . $feltetelepuletek[1] . ". szintjével";
                    }
                }
            }
        }

        foreach($nyersanyag as $key=>$value){
            foreach($feltelek as $key1=>$value1){

                if($key.'_koltseg'==$key1){

                    if(!($value>=$value1))
                        return "Nem rendelkezel ellegendő nyersanyaggal";
                }

            }

        }

        $seged=explode(':',$feltelek['epetesiido']);
        $timestamp=$seged[2]+$seged[1]*60+$seged[0]*60*60;
        $datetime=new DateTime();
        $datetime->setTimestamp($datetime->getTimestamp()+$timestamp);

        foreach($nyersanyag as $key=>$value){
            foreach($feltelek as $key1=>$value1){

                if($key."_koltseg"==$key1){
                    $nyersanyag[$key]-=$value1;
                }
            }

        }

        $kapcsolat=new mysqlkapcs();
        $kapcsolat->jatekosnyersbeal($this->id,$nyersanyag);
        $kapcsolat->jatekosepites($this->id,$feltelek['azon'],$epuletnev,$datetime->format('Y:m:d H:i:s'));

    }

    public function epuletkesz(){
        $this->kapcsolat->epites_kesz($this->id);
    }

}