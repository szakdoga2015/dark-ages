<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.05.04.
 * Time: 17:21
 */

require_once "../models/jatekos.php";
require_once "mysqlkapcs.php";

class tamadascontrol {

    private $jatekos;
    private $db;

    public function __construct($id){
        $this->jatekos=new jatekos($id);
        $this->db=new mysqlkapcs();
    }

    public function erkezes($poz){

        $anegyzet=pow($this->jatekos->poz()['poz_x']-$poz['poz_x'],2);
        $bnegyzet=pow($this->jatekos->poz()['poz_y']-$poz['poz_y'],2);

        $c=sqrt($anegyzet+$bnegyzet);
        $c=ceil($c)/2;

        $timestamp=60*60*$c;
        $datetime=new DateTime();
        $datetime->setTimestamp($datetime->getTimestamp()+$timestamp);

        return $datetime->format('Y:m:d H:i:s');

    }

    public function tamadhato_emberek(){
        $stmt=$this->db->dbc->prepare("SELECT id,nev,poz_x,poz_y from dark_ages.jatekos where id!=?");
        $stmt->execute(array($this->jatekos->id));
        $jatekosok=$stmt->fetchAll(PDO::FETCH_ASSOC);

        for($i=0;$i<count($jatekosok);$i++){
            $poz=array("poz_x"=>$jatekosok[$i]['poz_x'],"poz_y"=>$jatekosok[$i]['poz_y']);
            $jatekosok[$i]['tamadasiido']=$this->erkezes($poz);
        }
        return $jatekosok;


    }

    public function tamadas($id,$tamadoegysegek){

        $jatekosegységei=$this->jatekos->egysegek();
        $jatekos=new jatekos($id);
        $maxteher=0;
        $erkezesido=$this->erkezes($jatekos->poz());

        if($jatekosegységei['kardos']>=$tamadoegysegek['kardos'] && $jatekosegységei['landzsas']>=$tamadoegysegek['landzsas']&&
           $jatekosegységei['ijjasz']>=$tamadoegysegek['ijjasz'] && $jatekosegységei['szeker']>=$tamadoegysegek['szeker']) {

            $stmt=$this->db->dbc->prepare("SELECT `nev`,`teherbiras` FROM dark_ages.`egysegek` WHERE 1 ");
            $stmt->execute();
            $teher=$stmt->fetchAll(PDO::FETCH_ASSOC);


            foreach($teher as $egys){
                foreach($tamadoegysegek as $key=>$value){
                    if($egys['nev']==$key){
                        $maxteher+=$egys['teherbiras']*$value;
                    }
                }

            }


            $stmt = $this->db->dbc->prepare("INSERT INTO dark_ages.`tamadas`(`jatekos_id_tamado`, `jatekos_id_vedekezo`,`tamado_kardos`, `tamado_ijasz`, `tamado_landzsas`, `tamado_szeker`,`erkezesiido`, `tamad`,`maxterhetoseg`)
                                            VALUES (                                ?,                        ?,              ?,              ?,                  ?,                ?,          ?,            ?,        ?)");

            $stmt->execute(array($this->jatekos->id,$id,
                $tamadoegysegek['kardos'],$tamadoegysegek['ijjasz'],$tamadoegysegek['landzsas'],$tamadoegysegek['szeker'],
                $erkezesido ,true,$maxteher));
            return $erkezesido;
        }else{
            return "Nincs elég egység";
        }
    }


    public function utazasvege(){

        $stmt=$this->db->dbc->prepare("select * from dark_ages.`tamadas` WHERE jatekos_id_tamado=? && erkezesiido<=now()");
        $stmt->execute(array($this->jatekos->id));
        $tamadasadatai=$stmt->fetch(PDO::FETCH_ASSOC);

        if($tamadasadatai['tamad']==true){

            /**
             * védekező egység le kérés
             */
            $stmt=$this->db->dbc->prepare("SELECT `kardos`, `landzsas`, `ijjasz` FROM dark_ages.`jatekos_has_egysegek` WHERE jatekos_id=?");
            $stmt->execute(array($tamadasadatai['jatekos_id_vedekezo']));
            $vedekezoegysegek=$stmt->fetch(PDO::FETCH_ASSOC);

            /**
             * védekő épület le kérérs
             */
            $stmt=$this->db->dbc->prepare("select *
                                           from
                                                (
                                                SELECT `vedelem` as fal_vedelem
                                                FROM dark_ages.fal
                                                join dark_ages.jatekos
                                                on azon=fal_azon
                                                WHERE dark_ages.jatekos.id=?
                                                ) as fal,
                                                (
                                                SELECT  `vedelem` as ortorony_vedelem,
                                                    	 tamadas as ortorony_tamadas
                                                FROM dark_ages.ortorony
                                                join dark_ages.jatekos
                                                on azon=fal_azon
                                                WHERE dark_ages.jatekos.id=?
                                                ) as torony
                                          ");
            $stmt->execute(array($tamadasadatai['jatekos_id_vedekezo'],$tamadasadatai['jatekos_id_vedekezo']));
            $vedekezo_epulet=$stmt->fetch(PDO::FETCH_ASSOC);

            /**
             * egysegek tamadas vedelem
             */
            $stmt= $this->db->dbc->prepare("SELECT nev,tamadas,vedelem
                                            FROM Dark_Ages.egysegek
                                            where id in (2,3,4)");
            $stmt->execute();
            $egysegek=$stmt->fetchAll(PDO::FETCH_ASSOC);

            $v_vedelmiero=$vedekezoegysegek['kardos']*5+$vedekezoegysegek['landzsas']*5+$vedekezoegysegek['ijjasz']*2+$vedekezo_epulet['fal_vedelem']+$vedekezo_epulet['ortorony_vedelem'];

            $t_tamadoero=$tamadasadatai['tamado_kardos']*3+$tamadasadatai['tamado_ijasz']*7+$tamadasadatai['tamado_landzsas']*5+$tamadasadatai['tamado_szeker']*5;

            if($v_vedelmiero<$t_tamadoero){

                $j=new jatekos($tamadasadatai['jatekos_id_vedekezo']);
                $nyers=$j->nyersanyag;
                if(array_sum($nyers)>$tamadasadatai['maxterhetoseg']){
                    $tamadasadatai['rabolt_buza']=$nyers['buza'];
                    $tamadasadatai['rabolt_ko']=$nyers['ko'];
                    $tamadasadatai['rabolt_vas']=$nyers['vas'];
                    $tamadasadatai['rabolt_fa']=$nyers['fa'];
                    $nyers['buza']=$nyers['ko']=$nyers['vas']=$nyers['fa']=0;
                    $m=new mysqlkapcs();
                    $m->jatekosnyersbeal($j->id,$nyers);
                    $tamadasadatai['tamado_kardos']=ceil($tamadasadatai['tamado_kardos']*0.8);
                    $tamadasadatai['tamado_ijasz']=ceil($tamadasadatai['tamado_ijasz']*0.8);
                    $tamadasadatai['tamado_landzsas']=ceil($tamadasadatai['tamado_landzsas']*0.8);


                }else{
                    $tamadasadatai['rabolt_buza']=ceil($nyers['buza']*0.75);
                    $tamadasadatai['rabolt_ko']=ceil($nyers['ko']*0.75);
                    $tamadasadatai['rabolt_vas']=ceil($nyers['vas']*0.75);
                    $tamadasadatai['rabolt_fa']=ceil($nyers['fa']*0.75);
                    $nyers['buza']=$nyers['buza']- $tamadasadatai['rabolt_buza'];
                    $nyers['ko']=$nyers['ko']- $tamadasadatai['rabolt_ko'];
                    $nyers['vas']=$nyers['vas']- $tamadasadatai['rabolt_vas'];
                    $nyers['fa']=$nyers['fa']- $tamadasadatai['rabolt_fa'];
                    $m=new mysqlkapcs();
                    $m->jatekosnyersbeal($j->id,$nyers);

                }

                $stmt=$this->db->dbc->prepare("UPDATE dark_ages.`tamadas` SET `tamado_kardos`=?,`tamado_ijasz`=?,`tamado_landzsas`=?,`tamado_szeker`=?,`erkezesiido`?,`tamad`=FALSE ,`rabolt_buza`=?,`rabolt_ko`=?,`rabolt_vas`=?,`rabolt_fa`=?, WHERE jatekos_id_tamado=? && jatekos_id_vedekezo=? ");
                $stmt->execute(array($tamadasadatai['tamado_kardos'],$tamadasadatai['tamado_ijasz'],$tamadasadatai['tamado_landzsas'],$tamadasadatai['tamado_szeker']),$tamadasadatai['erkezesiido'],$tamadasadatai['rabolt_buza'],$tamadasadatai['rabolt_ko'],$tamadasadatai['rabolt_vas'],$tamadasadatai['rabolt_fa'],$tamadasadatai['jatekos_id_tamado'],$tamadasadatai['jatekos_id_vedekezo']);


                $stmt= $this->db->dbc->prepare("UPDATE dark_ages.`jatekos_has_egysegek`
                                                SET `kardos`=0,`landzsas`=0,`ijjasz`=0
                                                WHERE jatekos_id=?");
                $stmt->execute(array($tamadasadatai['jatekos_id_vedekezo']));

            }else{
                $tamadasadatai['tamado_kardos']=0;
                $tamadasadatai['tamado_ijasz']=0;
                $tamadasadatai['tamado_landzsas']=0;
                $tamadasadatai['tamado_szeker']=0;

                $j=new jatekos($tamadasadatai['jatekos_id_vedekezo']);
                $tamadasadatai['erkezesiido']=$this->erkezes($j->poz());

                $stmt=$this->db->dbc->prepare("UPDATE dark_ages.`tamadas` SET `tamado_kardos`=?,`tamado_ijasz`=?,`tamado_landzsas`=?,`tamado_szeker`=?,`erkezesiido`?,`tamad`=FALSE ,`rabolt_buza`=0,`rabolt_ko`=0,`rabolt_vas`=0,`rabolt_fa`=0, WHERE jatekos_id_tamado=? && jatekos_id_vedekezo=? ");
                $stmt->execute(array($tamadasadatai['tamado_kardos'],$tamadasadatai['tamado_ijasz'],$tamadasadatai['tamado_landzsas'],$tamadasadatai['tamado_szeker']),$tamadasadatai['erkezesiido'],$tamadasadatai['jatekos_id_tamado'],$tamadasadatai['jatekos_id_vedekezo']);

            }






        }else{
            $stmt=$this->db->dbc->prepare('UPDATE dark_ages.`jatekos_has_egysegek`
                                           SET `kardos`=?,`landzsas`=?,`ijjasz`=?,`szeker`=?
                                           WHERE jatekos_id=?');
            $stmt->execute(array($tamadasadatai['tamado_kardos'],$tamadasadatai['tamado_landzsas'],$tamadasadatai['tamado_ijasz']),$tamadasadatai['tamado_szeker']);

            $stmt=$this->db->dbc->prepare("UPDATE `jatekos_nyersanyag` SET `fa`=?,`ko`=?,`vas`=?,`buza`=? WHERE jatekos_id=?");
            $stmt->execute(array($tamadasadatai['rabolt_fa'],$tamadasadatai['rabolt_ko'],$tamadasadatai['rabolt_vas'],$tamadasadatai['rabolt_buza'],$tamadasadatai['jatekos_id_tamado']));

        }


    }





}
