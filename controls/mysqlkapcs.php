<?php
include 'defineadatb.php';


class mysqlkapcs{
	public $dbc;


	public function __construct(){
		try{

			$this->dbc=new PDO(dsn, dbuser, dbpass);
		}catch (PDOException $e){
			echo 'Nem sikerült csatlakozni'.$e->getMessage();
		}
	}

    public function felhaszletezik($nev){
        $stmt=$this->dbc->prepare("select id from jatekos where nev=?");
        $nev=array($nev);
        $stmt->execute($nev);
        return $stmt->rowCount();
    }

    public function felhasznaloregisztralas($nev,$jelszo,$email,$aktivalokod){
        try{

            $stmt=$this->dbc->prepare("SELECT * FROM Dark_Ages.terkep ORDER BY rand() LIMIT 1;");
            $stmt->execute();
            $poz=$stmt->fetch(PDO::FETCH_NUM);


            $stmt= $this->dbc->prepare("DELETE FROM Dark_Ages.terkep WHERE poz_x= ? and poz_y= ?;");
            $stmt->execute($poz);

            $stmt=$this->dbc->prepare("INSERT INTO Dark_Ages.jatekos  (nev,jelszo,email,aktiv,poz_x,poz_y)VALUES (?,?,?,?,?,?)");
            $adat=array_merge(array($nev,$jelszo,$email,$aktivalokod),$poz);

            $stmt->execute($adat);

            $stmt=$this->dbc->prepare("select id from Dark_Ages.jatekos where nev=?");
            $nev=array($nev);
            $stmt->execute($nev);
            $id=$stmt->fetch(PDO::FETCH_ASSOC);


            $stmt=$this->dbc->prepare("INSERT INTO Dark_Ages.jatekos_nyersanyag  (jatekos_id)VALUES (?)");
            $adat=array($id['id']);
            $stmt->execute($adat);

            $stmt=$this->dbc->prepare("INSERT INTO Dark_Ages.jatekos_has_egysegek  (jatekos_id)VALUES (?)");
            $adat=array($id['id']);
            $stmt->execute($adat);


        }catch (PDOException $e){
            echo 'Nem sikerült csatlakozni'.$e->getMessage();
        }
    }

    public function bejelentkezes($nev){
        $stmt=$this->dbc->prepare("select jelszo,id from Dark_Ages.jatekos where nev=?");
        $stmt->execute(array($nev));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function kovetkezoszintkoltseg($jatekosid,$epuletnev){

        $fromparam=$epuletnev;
        $jatekosparam='jatekos.'.$epuletnev.'_azon';
        $epuletparam=$epuletnev.'.azon';
        $query='select kovetkezoszint from '.$epuletnev.' join jatekos on '.$epuletparam.'='.$jatekosparam;
        //echo $query;
        try {
            $stmt = $this->dbc->prepare($query);

            $stmt->execute();

            $kovszint = $stmt->fetchColumn(0);

            $query='select * from '.$fromparam.' where azon=?';
            $stmt=$this->dbc->prepare($query);
            $stmt->execute(array($kovszint));

            return $stmt->fetch(PDO::FETCH_ASSOC);


        }catch (PDOException $e){
            echo ($e->errorInfo);
        }

    }

    public function jatekosEpuletek($id){

            $stmt = $this->dbc->prepare("select laktanya_azon,var_azon,kutato_azon,piac_azon,favago_azon,kobanya_azon,farm_azon,ercbanya_azon,
                                                lakohaz_azon,ortorony_azon,fal_azon,kocsma_azon,katedralis_azon,kovacs_azon
                                       from Dark_ages.jatekos
                                       where id=?");
            $stmt->execute(array($id));

            $seged=$stmt->fetch(PDO::FETCH_ASSOC);
            $epuletek=array();

            foreach($seged as $key=>$value){
                    $epuletek[explode("_",$key)[0]]=$value;

            }

           return $epuletek;
    }

    public function jatekosNyersanyag($id){
        $stmt=$this->dbc->prepare("select * from Dark_Ages.jatekos_nyersanyag where jatekos_id=?");

        $stmt->execute(array($id));

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function jatekosepites($jatekosid,$epuletid,$tipus,$elkezules){
        $stmt=$this->dbc->prepare("insert into Dark_Ages.epitesfolyamatban(jatekos_azon,epulet_azon,tipus,elkeszules)
                                    VALUES (?,?,?,?)");

        $stmt->execute(array($jatekosid,$epuletid,$tipus,$elkezules));

    }

    public function jatekosnyersbeal($jatekosid,$nyersanyag){

        $seged=" ";
        foreach($nyersanyag as $key=>$values){

            $seged.=$key.'='.$values.', ';

        }
        $seged=trim($seged,', ');
        $stmt="update dark_ages.jatekos_nyersanyag set ";
        $where=" where jatekos_id=".$jatekosid.";";

        $query=$this->dbc->query(($stmt.$seged.$where));



    }

    public function epites_kesz($jatekosid){
        $stmt=$this->dbc->prepare('call Dark_Ages.epiteskesz(?)');
        $stmt->execute(array($jatekosid));
    }

    public function aktivalas($kod){
        $stmt=$this->dbc->prepare("UPDATE Dark_Ages.jatekos set aktiv=now() where aktiv=? ");
        $stmt->execute(array($kod));
        return $stmt->queryString;
    }


}


?>


