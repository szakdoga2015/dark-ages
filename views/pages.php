<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.03.26.
 * Time: 10:41
 */
?>

<div id="bejelentkezes">
    <div id="fejlec">
        Bejelentkezés
    </div>

    <?php require_once "../controls/bejelentkezes.php" ?>

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post"  >
        <fieldset>
            <p><label>Név :</label>
            <input type="text" name="nev" id="nev"></p>
            <p><label>Jelszó :</label>
            <input type="password" name="jelszo" id="jelszo"></p>
            <br>
            <input type="submit" value="Bejelentkezes">
            <br>
        </fieldset>
    </form>
    <br><br>
</div>


<div id="hirek">
    <div id="fejlec">
        Hírek
    </div>
    <div id="newhir">
        <h1 class="hhir">Fejlesztés</h1>
        <p class="szhir">
            A Dark Ages gőzerővel fejlődik napról napra, hogy egy kellemes játék jöjjön létre.
            Teszteljünk is kicsit:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget vehicula eros. Praesent varius elementum ullamcorper. Vestibulum sed facilisis massa.
            Nullam posuere ultricies ullamcorper. Pellentesque sagittis ante eu nisl tempus mollis a ut ipsum.
            Aliquam facilisis egestas magna, nec ullamcorper sapien consectetur vitae. Sed fermentum egestas fringilla. Etiam eleifend sollicitudin felis ut facilisis.
        </p>
    </div>
    <hr>
    <div id="newhir">
        <h1 class="hhir">Itt hírek lesznek</h1>
        <p class="szhir">
            A Dark Ages fejlődik.
            Teszteljünk is kicsit:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget vehicula eros. Praesent varius elementum ullamcorper. Vestibulum sed facilisis massa.
            Nullam posuere ultricies ullamcorper. Pellentesque sagittis ante eu nisl tempus mollis a ut ipsum.
            Aliquam facilisis egestas magna, nec ullamcorper sapien consectetur vitae. Sed fermentum egestas fringilla. Etiam eleifend sollicitudin felis ut facilisis.
        </p>
    </div>
    <br><br>
</div>


<div id="kontakt">
    <div id="fejlec">
        Kontakt
    </div>
    <div id="newhir">
        <p class="szhir">
            <h2>Fejlesztők :</h2>
            Kiss Zoltán - NeptunKód<br>
            Halmai Bianka Zsófia - NeptunKód

            <h2>Elérhetőség:</h2>
            Email : vmi@vmi.hu
        </p>
    </div>
    <br><br>
</div>


<div id="regisztracio">
    <div id="fejlec">
        Regisztráció
    </div>

    <?php
    require_once "../controls/regisztracio.php"
    ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post"  >

        <div>
            <label for="nev">Név:</label>
            <?php echo $neverr."<br>" ?>
            <input type="text" name="nev" id="nev" value="<?php echo $nev;?>"><br>
        </div>
        <br>

        <div>
            <label for="email">e-mail:</label>
            <?php echo $emailerr."<br>" ?>
            <input type="email" name="email" id="email" value="<?php echo $email;?>">
        </div>
        <br>

        <div>
            <label for="jelszo">Jelszó:</label>
            <?php echo $jelszoerr."<br>" ?>
            <input type="password" name="jelszo" id="jelszo"><br>
        </div>
        <br>

		<div class="g-recaptcha" data-sitekey="6LfaCgYTAAAAAD_cfvGCkMMJ-QoGFabG9NjkSX4F"></div>
		
        <br>
        <p id="newhir">
        <input type="submit" value="regisztráció">
        </p>
		
    </form>
	
    <br><br>
</div>

