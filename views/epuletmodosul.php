<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.05.05.
 * Time: 11:32
 */

header('Content-Type: text/html; charset=utf-8');

session_start();
require_once "../views/ujraHasznalando.php";
require_once "../controls/jatek.php";
require_once "../controls/kereskedes.php";
require_once "../controls/egyseg_gyartas.php";
require_once "../controls/fegyvergyartas.php";
require_once "../controls/tamadascontrol.php";

$kereskedes = new kereskedes($_SESSION['id']);
$jatek = new jatek( $_SESSION['id'] );
$ujra = new ujraHasznalando();
$tamad = new tamadascontrol($_SESSION['id']);


$tamad->utazasvege();$tamad->utazasvege();
$osszEpulet = $ujra -> OsszEpulet();

$jatek->frisitnyers();
$jatek->epuletkesz();

$id = $_GET['id'];
$lenneEpulet = $_GET['epul'];

if( isset($_POST['fejlesz1'])  ) {


    if ($id != -1) {

        foreach ($osszEpulet as $kulcs => $ertek) {
            if ($id == $kulcs)
                $epulet = $ertek[0];
        }
    } else {
        foreach ($osszEpulet as $kulcs => $ertek) {
            if ($lenneEpulet == $kulcs)
                $epulet = $ertek[0];
        }
    }
    $üzi = $jatek->kovszintepit($epulet);

    if (empty($üzi)) {
        echo "<script type='text/javascript'>alert('Sikeres fejlesztés!');</script>";
    } else
        echo "<script type='text/javascript'>alert('$üzi');</script>";
}
else if( isset($_POST['Pegyseg'])  ) {

    if (isset($_POST['kereskedo_vesz']))
        $kereskedo_vesz = $_POST['kereskedo_vesz'];
    else
        $kereskedo_vesz = 0;

    if (isset($_POST['szeker_vesz']))
        $szeker_vesz = $_POST['szeker_vesz'];
    else
        $szeker_vesz = 0;

    if ($szeker_vesz != 0 || $kereskedo_vesz != 0) {
        $egysegek = array("kereskedo" => $kereskedo_vesz, "szeker" => $szeker_vesz);

        $egyseg = new egyseg_gyartas($_SESSION['id']);
        $üzi = $egyseg->egyseggyartas_piac($egysegek);

        if (empty($üzi)) {
            echo "<script type='text/javascript'>alert('Sikeres egységvásárlás!');</script>";
        } else
            echo "<script type='text/javascript'>alert('$üzi');</script>";
    }
}
else if( isset( $_POST['Legyseg'] ) )
{
    if( isset($_POST['ijjasz_vesz']) )
        $ijasz_vesz = $_POST['ijjasz_vesz'];
    else
        $ijasz_vesz = 0;

    if( isset($_POST['kardos_vesz']) )
        $kardos_vesz = $_POST['kardos_vesz'];
    else
        $kardos_vesz = 0;

    if( isset($_POST['landzsas_vesz']) )
        $landzs_vesz = $_POST['landzsas_vesz'];
    else
        $landzs_vesz = 0;

    $egysegek = array( "kardos"=> $kardos_vesz, "ijjasz"=>$ijasz_vesz, "landzsas"=>$landzs_vesz );
    //print_r($egysegek);

    $egyseg = new egyseg_gyartas($_SESSION['id']);
    $üzi = $egyseg->egysegygartas_laktanya($egysegek);

    if( empty($üzi) ) {
        echo "<script type='text/javascript'>alert('Sikeres egységgyártás!');</script>";
    }
    else
        echo "<script type='text/javascript'>alert('$üzi');</script>";
}
else if( isset( $_POST["Kfegyver"] ) )
{
    if( isset($_POST['ij_vesz']) )
        $ij = $_POST['ij_vesz'];
    else
        $ij = 0;

    if( isset($_POST['kard_vesz']) )
        $kard = $_POST['kard_vesz'];
    else
        $kard = 0;

    if( isset($_POST['landzsa_vesz']) )
        $landzsa = $_POST['landzsa_vesz'];
    else
        $landzsa = 0;

    $fegyverek = array( "termeles_kard"=>$kard, "termeles_ij"=>$ij, "termeles_landzsa"=>$landzsa );

    $fegyvergyartas = new szerszamgyartas($_SESSION['id']);

    $üzi = $fegyvergyartas->fegyvergyartas($fegyverek);

    if( empty($üzi) ) {
        echo "<script type='text/javascript'>alert('Sikeres fegyvergyártás!');</script>";
    }
    else
        echo "<script type='text/javascript'>alert('$üzi');</script>";
}
else if( isset( $_POST["Ltamadas"] ) )
{
    if( isset($_POST['kardos_kuld']) )
        $kardos = $_POST['kardos_kuld'];
    else
        $kardos = 0;

    if( isset($_POST['ijasz_kuld']) )
        $ijasz = $_POST['ijasz_kuld'];
    else
        $ijasz = 0;

    if( isset($_POST['landzsas_kuld']) )
        $landzsas = $_POST['landzsas_kuld'];
    else
        $landzsas = 0;

    if( isset($_POST['szeker_kuld']) )
        $szeker = $_POST['szeker_kuld'];
    else
        $szeker = 0;

    $jatekos = $_POST['tamad'];

    $egysegek = array( "kardos"=>$kardos, "ijjasz"=>$ijasz, "landzsas"=>$landzsas, "szeker"=>$szeker );

    $üzi = $tamad->tamadas($jatekos, $egysegek);

    echo "<script type='text/javascript'>alert('$üzi');</script>";
}
else if( isset($_POST["keresked"]) )  {

    if( isset($_POST['nyilt']) )
    {
        $vesz = $_POST['nyilt'];
        $ido = $kereskedes->ajanlatelfogad($vesz);
    }

    include "../views/tarolando.php";
    if( isset($_POST['Kínál']) )
    {
        $kínál = $_POST['Kínál'];
        $nyilt = $kereskedes->kinal($kínál);
    }
    else if( isset( $_POST['Keres']) && $_POST['Keres'] != null )
    {
        $keres = $_POST['Keres'];
         $nyilt = $kereskedes->kinal($keres);
    }
    else
        $nyilt = "";

    if( isset( $_POST['Mit']) && $_POST['Mit'] != null  )
    {
        if( isset( $_POST['Mire']) && $_POST['Mire'] != null ) {

            $mire = $_POST['Mire'];
            $mire_mennyi = $_POST['mire_mennyi'];
            $mire_kesz = array( "buza" => "0", "fa" => "0", "vas" => "0", "ko" => "0" );

            foreach ($mire_kesz as $key => $value)
            {
                if( $key == $mire )
                    $mire_kesz[$key] = $mire_mennyi;
            }

            $nyers = $jatek->getnyersanyag();
            $mit = $_POST['Mit'];
            $mit_mennyi = $_POST['mit_mennyi'];

            $mit_kesz = array("buza" => "0", "fa" => "0", "vas" => "0", "ko" => "0");

            foreach ($mit_kesz as $key => $value) {
                if ($key == $mit)
                    $mit_kesz[$key] = $mit_mennyi;
            }

            if( $mit_mennyi !=0 && $mire_mennyi !=0 ) {
                $ajanlat = new kereskedes($_SESSION['id']);
                $üzi = $ajanlat->ajanlattesz($mit_kesz, $mire_kesz);

                if (empty($üzi)) {
                    echo "<script type='text/javascript'>alert('Sikeres ajánlat tevés!');</script>";
                } else
                    echo "<script type='text/javascript'>alert('$üzi');</script>";
            }
        }
        else
            echo "<script type='text/javascript'>alert('Ha el akarsz addni, töltsd ki minden mezőt az eladásnál!');</script>";
    }
    elseif( isset( $_POST['Mire']) && $_POST['Mire'] != null )
        echo "<script type='text/javascript'>alert('Ha el akarsz addni, töltsd ki minden mezőt az eladásnál!');</script>";
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="epuletmodosul.css">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script>

        $( document ).ready(function() {
            var alapw = 1920;
            var aranyW = window.outerWidth/alapw;

            $( "#bKep").css( "width", parseInt($( "#bKep").css( "width")) * aranyW);

            $("#koltseg").css("fontSize", parseInt($("#koltseg").css("fontSize"))*aranyW);
            $("#koltseg h2").css("fontSize", parseInt($("#koltseg h2").css("fontSize"))*aranyW);

            $( "#pergament").css( "width", parseInt($( "#pergament").css( "width")) * aranyW);
            $("#pergament h1").css("fontSize", parseInt($("#pergament h1").css("fontSize"))*aranyW);

            //$("#nyilt_kereskedo_felulet").css("fontSize", parseInt($("#nyilt_kereskedo_felulet").css("fontSize"))*aranyW);
            //$("#kereskedo_felulet").css("fontSize", parseInt($("#kereskedo_felulet").css("fontSize"))*aranyW);
        });

    </script>
</head>
<body>

<div id="pergament">
    <?php
    //piac
    if( $id == 8 )
    {
        ?>
        <ul id="nav">
            <li><a href="ajax_adat.php?id=8&epul=0">Adatok</a></li>
            <li><a href="piac_vasarlas.php?id=8&epul=0">Kereskedelem</a></li>
            <li><a href="piac_egyseg_gyartas.php?id=8&epul=0">Egység gyártás</a></li>
        </ul>

        <div id="content" ></div>
        <script src="../views/piac.js"></script>
        <?php
    }
    //laktanya
    elseif( $id == 7 )
    {
        ?>
        <ul id="nav">
            <li><a href="ajax_adat.php?id=7&epul=0">Adatok</a></li>
            <li><a href="laktanya_egyseg_gyartas.php?id=7&epul=0">Egység gyártás</a></li>
            <li><a href="laktanya_tamadas.php?id=7&epul=0">Támadás</a></li>
        </ul>

        <div id="content" ></div>
        <script src="../views/laktanya.js"></script>
        <?php
    }
    //kovács
    elseif( $id == 6 )
    {
        ?>
        <ul id="nav">
            <li><a href="ajax_adat.php?id=6&epul=0">Adatok</a></li>
            <li><a href="kovacs_fegyver_gyartas.php?id=6&epul=0">Fegyver gyártás</a></li>
        </ul>

        <div id="content" ></div>
        <script src="../views/kovacs.js"></script>
        <?php
    }
    else if( $id != -1 )
    {
        include 'epulet_adat.php';
        ?>
        <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet;?>" method="post" style="padding-bottom: 10%;">
        <input type="submit" name="fejlesz1" value="Fejleszt">
        <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'">
        </form>
        <?php
    }
    else
    {
    $nev = "Üres területre építés";

    foreach ($osszEpulet as $kulcs => $ertek) {
        if ($lenneEpulet == $kulcs)
            $epulet = $ertek[0];
    }

    $lenne = $ujra->NevEpulet($epulet);

    $kep = "../resources/images/buildings/" . $epulet . ".png";
    $kovnyers = $jatek->kovnyers($epulet);

    ?>
    <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet;?>"method="post">
        <h1><?php echo $nev; ?></h1>
        <img id="bKep" src="<?php echo $kep; ?>">

        <div id="koltseg">
            <h2>Ide építhető épület: <span class="piros"><?php echo $lenne?></span></h2>
            <h2>Fejlesztes költségei:</h2>
            Fa: <span class="piros"><?php print_r($kovnyers['fa_koltseg']) ?></span><br>
            Kő: <span class="piros"><?php print_r($kovnyers['ko_koltseg']) ?></span><br>
            Búza: <span class="piros"><?php print_r($kovnyers['buza_koltseg']) ?></span><br>
            Vas: <span class="piros"><?php print_r($kovnyers['vas_koltseg']) ?></span><br>
            Minimum feltétel: <span class="piros"><?php print_r($kovnyers['feltetel']) ?></span><br>
            Fejlesztési idő: <span class="piros"><?php echo $kovnyers['epetesiido'] ?></span><br>
            <?php
            if( isset($kovnyers['termeles']) )
            {
                echo "Fejlesztéssel való népesség növekedés: <span class=\"piros\">";
                echo $kovnyers['termeles'];
                echo "</span><br>";
            }
            ?>
            Következő szint: <span class="piros"><?php print_r($kovnyers['kovetkezoszint']) ?></span><br><br>

            <input type="submit" name="fejlesz1" value="Fejleszt">
            <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'">
            <br><br>
        </div>
    </form>
    <?php
    }
    ?>
</div>

</body>