<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.05.05.
 * Time: 20:11
 */

session_start();
require_once "../controls/egyseg_gyartas.php";

$egyseg = new egyseg_gyartas($_SESSION['id']);
$hany = $egyseg->gyarthatoegyseg_laktanya();

$id = $_GET['id'];
$lenneEpulet = $_GET['epul'];
?>

<head>
    <style>
        #pergament{
            height: 900px;
        }
    </style>
</head>

<form action="<?php echo '../views/epuletmodosul.php?id=' . $id . '&epul=' . $lenneEpulet;?>" method="post">
    <div id="laktanya">
        <br>
        <h1 style="font-size: 40px"> Egységek: </h1>

        <?php
        $v = array("ijjasz", "kardos", "landzsas" );
        for( $i=0; $i<3; $i++ )
        {
            echo "<span style=\"font-weight: bold; text-shadow: 1px 1px black; font-size: 30px; text-decoration: underline;\">";
            if( $v[$i] == "ijjasz" ) echo "Íjász:"; if( $v[$i] == "kardos" ) echo "Kardos:"; if( $v[$i] == "landzsas" ) echo "Lándzsás:";
            echo "</span><br><br>
                                Vásárolható: <span class=\"piros\">".$hany[$v[$i]]["epitheto"]."</span> <br>
                                Támadás: <span class=\"piros\">".$hany["$v[$i]"]["tamadas"]."</span>
                                Védelem: <span class=\"piros\">".$hany["$v[$i]"]["vedelem"]."</span>
                                <br>
                                <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">Ár: </span>";
            if( $hany["$v[$i]"]['koltseg_kard'] !=0 ){ echo "Kard: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_kard"]."  </span>"; }
            if( $hany["$v[$i]"]['koltseg_ij'] !=0 ){ echo "Íj: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_ij"]."  </span>"; }
            if( $hany["$v[$i]"]['koltseg_landzsa'] !=0 ){ echo "Lándzsa: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_landzsa"]."  </span>"; }
            if( $hany["$v[$i]"]['buza'] !=0 ){ echo "Búza: "."<span class=\"piros\">".$hany["$v[$i]"]["buza"]." </span>"; }
            if( $hany["$v[$i]"]['fa'] !=0 ){ echo "Fa: "."<span class=\"piros\">".$hany["$v[$i]"]["fa"]."  </span>"; }
            if( $hany["$v[$i]"]['teherbiras'] !=0 ){ echo "Teherbírás: "."<span class=\"piros\">".$hany["$v[$i]"]["teherbiras"]."  </span>"; }
            echo "<br>
                                <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">";
            if( $v[$i] == "ijjasz" ) echo "Íjász vásárlás: "; if( $v[$i] == "kardos" ) echo "Kardos vásárlás: "; if( $v[$i] == "landzsas" ) echo "Lándzsás vásárlás: ";
            echo "</span>
                                <input type=\"number\" name=\"$v[$i]_vesz\" min=\"0\" max=\"".$hany["$v[$i]"]["epitheto"]."\" step=\"1\">
                                <br><br>";
        }
        ?>
        <input type="submit" name="Legyseg" value="Küldés" style="float: left">
        <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'" style="float: left; margin-left: 10px ">
    </div>
</form>