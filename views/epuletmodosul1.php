<?php
header('Content-Type: text/html; charset=utf-8');
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.04.05.
 * Time: 15:11
 */

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Dark Ages</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="epuletmodosul.css">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script>

        $( document ).ready(function() {
            var alapw = 1920;

            var aranyW = window.outerWidth/alapw;

            $( "#bKep").css( "width", parseInt($( "#bKep").css( "width")) * aranyW);
            $("#koltseg").css("fontSize", parseInt($("#koltseg").css("fontSize"))*aranyW);
            $("#koltseg h2").css("fontSize", parseInt($("#koltseg h2").css("fontSize"))*aranyW);
            $("h1").css("fontSize", parseInt($("h1").css("fontSize"))*aranyW);

            $("#nyilt_kereskedo_felulet").css("fontSize", parseInt($("#nyilt_kereskedo_felulet").css("fontSize"))*aranyW);
            $("#kereskedo_felulet").css("fontSize", parseInt($("#kereskedo_felulet").css("fontSize"))*aranyW);
        });

    </script>
</head>
<body>
    <div id="pergament">
        <img id="papir" src="../resources/images/papir.jpg" alt="Dark Ages">


        <?php
            session_start();
            require_once "../views/ujraHasznalando.php";
            require_once "../controls/jatek.php";
            require_once "../controls/kereskedes.php";
            require_once "../models/jatekos.php";
            require_once "../controls/egyseg_gyartas.php";
            require_once "../controls/fegyvergyartas.php";

            $kereskedes = new kereskedes($_SESSION['id']);
            $jatek = new jatek( $_SESSION['id'] );

            $id = $_GET['id'];
            $lenneEpulet = $_GET['epul'];
            $jatek->frisitnyers();
            $jatek->epuletkesz();

        if( $_SERVER["REQUEST_METHOD"] == "POST"  ) {


            if($id!=-1){

                foreach ($osszEpulet as $kulcs => $ertek) {
                    if ($id == $kulcs)
                        $epulet = $ertek[0];
                }
            }else{
                foreach ($osszEpulet as $kulcs => $ertek) {
                    if ($lenneEpulet == $kulcs)
                        $epulet = $ertek[0];
                }
            }


            if( isset( $_GET['egyseg'] ) )
            {
                if( isset($_POST['ijjasz_vesz']) )
                    $ijasz_vesz = $_POST['ijjasz_vesz'];
                else
                    $ijasz_vesz = 0;

                if( isset($_POST['kardos_vesz']) )
                    $kardos_vesz = $_POST['kardos_vesz'];
                else
                    $kardos_vesz = 0;

                if( isset($_POST['landzsas_vesz']) )
                    $landzs_vesz = $_POST['landzsas_vesz'];
                else
                    $landzs_vesz = 0;

                $egysegek = array( "kardos"=> $kardos_vesz, "ijjasz"=>$ijasz_vesz, "landzsas"=>$landzs_vesz );
                //print_r($egysegek);

                $egyseg = new egyseg_gyartas($_SESSION['id']);
                $üzi = $egyseg->egysegygartas_laktanya($egysegek);

                if( empty($üzi) ) {
                    echo "<script type='text/javascript'>alert('Sikeres egységgyártás!');</script>";
                }
                else
                    echo "<script type='text/javascript'>alert('$üzi');</script>";


            }
            elseif( isset( $_GET['fegyver'] ) )
            {

                if( isset($_GET['ij_vesz']) )
                    $ij = $_GET['ij_vesz'];
                else
                    $ij = 0;

                if( isset($_GET['kard_vesz']) )
                    $kard = $_GET['kard_vesz'];
                else
                    $kard = 0;

                if( isset($_GET['landzsa_vesz']) )
                    $landzsa = $_GET['landzsa_vesz'];
                else
                    $landzsa = 0;

                $fegyverek = array( "termeles_kard"=>$kard, "termeles_ij"=>$ij, "termeles_landzsa"=>$landzsa );

                $fegyvergyartas = new szerszamgyartas($_SESSION['id']);

                $üzi = $fegyvergyartas->fegyvergyartas($fegyverek);

                if( empty($üzi) ) {
                    echo "<script type='text/javascript'>alert('Sikeres fegyvergyártás!');</script>";
                }
                else
                    echo "<script type='text/javascript'>alert('$üzi');</script>";


            }
            else if( isset( $_GET['keresked'] ) )
            {
                if( isset($_POST['nyilt']) )
                {
                    $vesz = $_POST['nyilt'];
                    echo $vesz;
                }

                if( isset($_POST['kereskedo_vesz']) )
                    $kereskedo_vesz = $_POST['kereskedo_vesz'];
                else
                    $kereskedo_vesz = 0;

                if( isset( $_POST['szeker_vesz'] ) )
                    $szeker_vesz = $_POST['szeker_vesz'];
                else
                    $szeker_vesz = 0;

                if( $szeker_vesz!=0 || $kereskedo_vesz!=0 )
                {
                    $egysegek = array( "kereskedo"=>$kereskedo_vesz, "szeker"=>$szeker_vesz );

                    $egyseg = new egyseg_gyartas($_SESSION['id']);
                    $üzi = $egyseg->egyseggyartas_piac($egysegek);

                    if( empty($üzi) ) {
                        echo "<script type='text/javascript'>alert('Sikeres egységvásárlás!');</script>";
                    }
                    else
                        echo "<script type='text/javascript'>alert('$üzi');</script>";
                }

                if( isset( $_POST['Kínál']) && $_POST['Kínál'] != null )
                {
                    $kínál = $_POST['Kínál'];
                    $nyilt = $kereskedes->kinal($kínál);
                }
                if( isset( $_POST['Keres']) && $_POST['Keres'] != null )
                {
                    $keres = $_POST['Keres'];
                    $nyilt = $kereskedes->keres($keres);
                }

                if( isset( $_POST['Mit']) && $_POST['Mit'] != null  )
                {
                    if( isset( $_POST['Mire']) && $_POST['Mire'] != null ) {

                        $mire = $_POST['Mire'];
                        $mire_mennyi = $_POST['mire_mennyi'];
                        $mire_kesz = array( "buza" => "0", "fa" => "0", "vas" => "0", "ko" => "0" );

                        foreach ($mire_kesz as $key => $value)
                        {
                            if( $key == $mire )
                                $mire_kesz[$key] = $mire_mennyi;
                        }

                        $nyers = $jatek->getnyersanyag();
                        $mit = $_POST['Mit'];
                        $mit_mennyi = $_POST['mit_mennyi'];

                        $mit_kesz = array("buza" => "0", "fa" => "0", "vas" => "0", "ko" => "0");

                        foreach ($mit_kesz as $key => $value) {
                            if ($key == $mit)
                                $mit_kesz[$key] = $mit_mennyi;
                        }

                        if( $mit_mennyi !=0 && $mire_mennyi !=0 ) {
                            $ajanlat = new kereskedes($_SESSION['id']);
                            $üzi = $ajanlat->ajanlattesz($mit_kesz, $mire_kesz);

                            if (empty($üzi)) {
                                echo "<script type='text/javascript'>alert('Sikeres ajánlat tevés!');</script>";
                            } else
                                echo "<script type='text/javascript'>alert('$üzi');</script>";
                        }
                    }
                    else
                        echo "<script type='text/javascript'>alert('Ha el akarsz addni, töltsd ki minden mezőt az eladásnál!');</script>";
                }
                elseif( isset( $_POST['Mire']) && $_POST['Mire'] != null )
                    echo "<script type='text/javascript'>alert('Ha el akarsz addni, töltsd ki minden mezőt az eladásnál!');</script>";

            }
            else{
                $üzi = $jatek->kovszintepit($epulet);

                if( empty($üzi) ) {
                    echo "<script type='text/javascript'>alert('Sikeres fejlesztés!');</script>";
                }
                else
                    echo "<script type='text/javascript'>alert('$üzi');</script>";
            }
        }
        if ($id != -1) {
            foreach ($osszEpulet as $kulcs => $ertek) {
                if ($id == $kulcs)
                    $epulet = $ertek[0];
            }

            $kep = "../resources/images/buildings/" . $epulet . ".png";

            $ujra = new ujraHasznalando();
            $nev = $ujra->NevEpulet($epulet);
            $nev .= " fejlesztése";

            $kovnyers = $jatek->kovnyers($epulet);
            ?>
            <div id="belso">
                <h1><?php echo $nev; ?></h1>
                <img id="bKep" src="<?php echo $kep; ?>">

                <div id="koltseg">
                    <h2>Fejlesztes költségei:</h2>
                    Fa: <span class="piros"><?php print_r($kovnyers['fa_koltseg']) ?></span><br>
                    Kő: <span class="piros"><?php print_r($kovnyers['ko_koltseg']) ?></span><br>
                    Búza: <span class="piros"><?php print_r($kovnyers['buza_koltseg']) ?></span><br>
                    Vas: <span class="piros"><?php print_r($kovnyers['vas_koltseg']) ?></span><br>
                    Minimum feltétel: <span class="piros"><?php print_r($kovnyers['feltetel']) ?></span><br>
                    Fejlesztési idő: <span class="piros"><?php print_r($kovnyers['epetesiido']) ?></span><br>
                    <?php
                    if( isset($kovnyers['termeles']) )
                    {
                        echo "Fejlesztéssel való népesség növekedés: <span class=\"piros\">";
                        echo $kovnyers['termeles'];
                        echo "</span><br>";
                    }
                    ?>
                    Következő szint: <span class="piros"><?php print_r($kovnyers['kovetkezoszint']) ?></span><br><br>

                    <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet;?>"
                          method="post">
                        <input type="submit" name="fejlesz" value="Fejleszt">
                        <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'">
                    </form>
                </div>

                <?php
                    if( $epulet == 'piac' ) {

                        if( !isset($nyilt) || $nyilt == null )
                            $nyilt = $kereskedes->nyiltajanlat();

                        //print_r($nyilt);

                        foreach( $nyilt as $key=>$value )
                        {
                            $split=explode("_",$key);
                            if( isset($split[1]) )
                            {
                                if( $split[1] == "ajanl" && $value>0 )
                                {
                                    $nyilt_mit = $split[0];
                                    $nyilt_mit_mennyi = $value;
                                }
                                else if( $split[1] == "keres" && $value>0 )
                                {
                                    $nyilt_mire = $split[0];
                                    $nyilt_mire_mennyi = $value;
                                }
                            }

                        }

                        $egyseg = new egyseg_gyartas($_SESSION['id']);
                        $hany = $egyseg->gyarthatoegysegek_piac();
                        //print_r($hany);

                        ?>
                        <br><br>
                        <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet.'&keresked=1';?>" method="post">
                            <div id="nyilt_kereskedo_felulet">
                                Vásárlás: Mit - Miért
                                <select name="nyilt" size="10" style="width: 100%">
                                    <option value="" selected></option>
                            <?php
                                foreach( $nyilt as $key =>$value )
                                {
                                    foreach( $value as $kulcs=>$ertek )
                                    {
                                        if( strpos($kulcs,'_') !== false )
                                        {
                                            $plit = explode("_", $kulcs);

                                            if( $ertek !=0 && $plit[1] == 'ajanl' )
                                            {
                                                $ajanlN = $plit[0];
                                                $ajanlE = $ertek;
                                            }

                                            if( $ertek !=0 && $plit[1] == 'keres' )
                                            {
                                                $keresN = $plit[0];
                                                $keresE = $ertek;
                                            }
                                        }
                                    }
                                    $nevek = array( 'buza'=>'Búza', 'fa'=>'Fa', 'vas'=>'Vas', 'ko'=>'Kő' );

                                    foreach( $nevek as $key=>$value )
                                    {
                                        if( $key == $ajanlN )
                                            $ajanlN = $value;

                                        if( $key == $keresN )
                                            $keresN = $value;
                                    }

                                    echo "<option value='$nyilt[$key]['id']'>$ajanlN: $ajanlE - $keresN: $keresE</option>";
                                }

                            ?>
                                </select>
                            </div>
                            <div id="kereskedo_felulet">
                                <br>
                                Kínál:
                                <select name="Kínál" size="1">
                                    <option value=""></option>
                                    <option value="buza">Búza</option>
                                    <option value="fa">Fa</option>
                                    <option value="vas">Vas</option>
                                    <option value="ko">Kő</option>
                                </select>
                                <br>
                                Keres:
                                <select name="Keres" size="1">
                                    <option value=""></option>
                                    <option value="buza">Búza</option>
                                    <option value="fa">Fa</option>
                                    <option value="vas">Vas</option>
                                    <option value="ko">Kő</option>
                                </select>
                                <br><br>
                                Elad:
                                <br>
                                Mit:
                                <select name="Mit" size="1">
                                    <option value=""></option>
                                    <option value="buza">Búza</option>
                                    <option value="fa">Fa</option>
                                    <option value="vas">Vas</option>
                                    <option value="ko">Kő</option>
                                </select>
                                Mennyiség: <input type="number" name="mit_mennyi" min="0" value="0" style="width: 25%;">
                                <br>
                                Mire:
                                <select name="Mire" size="1">
                                    <option value=""></option>
                                    <option value="buza">Búza</option>
                                    <option value="fa">Fa</option>
                                    <option value="vas">Vas</option>
                                    <option value="ko">Kő</option>
                                </select>
                                Mennyiség: <input type="number" name="mire_mennyi" min="0" value="0" style="width: 25%;">
                                <br><br><br>
                            </div>
                            <div id="laktanya">
                                <br><br>
                                <h1 style="font-size: 30px"> Egységek: </h1>
                                <?php
                                $v = array("kereskedo", "szeker" );
                                for( $i=0; $i<2; $i++ )
                                {
                                    echo "<span style=\"font-weight: bold; text-shadow: 1px 1px black; font-size: 20px;\">";
                                    if( $v[$i] == "kereskedo" ) echo "Kereskedő: "; if( $v[$i] == "szeker" ) echo "Szekér: ";
                                    echo "</span><br>
                                    Vásárolható: <span class=\"piros\">".$hany[$v[$i]]["epitheto"]."</span> <br>
                                    Támadás: <span class=\"piros\">".$hany["$v[$i]"]["tamadas"]."</span>
                                    Védelem: <span class=\"piros\">".$hany["$v[$i]"]["vedelem"]."</span>
                                    <br>
                                    <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">Ár: </span>";
                                    if( $hany["$v[$i]"]['koltseg_kard'] !=0 ){ echo "Kard: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_kard"]."  </span>"; }
                                    if( $hany["$v[$i]"]['koltseg_ij'] !=0 ){ echo "Íj: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_ij"]."  </span>"; }
                                    if( $hany["$v[$i]"]['koltseg_landzsa'] !=0 ){ echo "Lándzsa: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_landzsa"]."  </span>"; }
                                    if( $hany["$v[$i]"]['buza'] !=0 ){ echo "Búza: "."<span class=\"piros\">".$hany["$v[$i]"]["buza"]." </span>"; }
                                    if( $hany["$v[$i]"]['fa'] !=0 ){ echo "Fa: "."<span class=\"piros\">".$hany["$v[$i]"]["fa"]."  </span>"; }
                                    if( $hany["$v[$i]"]['teherbiras'] !=0 ){ echo "Teherbírás: "."<span class=\"piros\">".$hany["$v[$i]"]["teherbiras"]."  </span>"; }
                                    echo "<br>
                                    <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">";
                                    if( $v[$i] == "kereskedo" ) echo "Kereskedő vásárlás: "; if( $v[$i] == "szeker" ) echo "Szekér vásárlás: ";
                                    echo "</span>
                                    <input type=\"number\" name=\"$v[$i]_vesz\" min=\"0\" max=\"".$hany["$v[$i]"]["epitheto"]."\" step=\"1\">
                                    <br><br>";
                                }
                                ?>
                                <input type="submit" name="kereskedés" value="Küldés" style="float: left">
                                <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'" style="float: left; margin-left: 10px">
                            </div>
                         </form>
                    <?php
                    }elseif( $epulet == 'kovacs' )
                    {
                        $fegyver = new szerszamgyartas($_SESSION['id']);
                        $szerszam = $fegyver->gyarthatofegyver();
                        $ar = $fegyver->fegyver;
                        $ar = array( "kard" => $ar[0], 'landzsa' => $ar[1], 'ij' => $ar[2] );

                        //print_r($szerszam);
                        //print_r($ar);

                        ?>
                        <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet.'&fegyver=1';?>" method="post">
                        <div id="laktanya">
                            <br>
                            <h1 style="font-size: 30px"> Fegyverek: </h1>

                            <?php
                            $v = array("kard", "ij", "landzsa" );
                            for( $i=0; $i<3; $i++ )
                            {
                                echo "<span style=\"font-weight: bold; text-shadow: 1px 1px black; font-size: 20px;\">";
                                if( $v[$i] == "kard" ) echo "Kard: "; if( $v[$i] == "ij" ) echo "Íj: "; if( $v[$i] == "landzsa" ) echo "Lándzsa: ";
                                echo "</span><br>Vásárolható: <span class=\"piros\">".$szerszam["termeles_".$v[$i]]."</span> <br>
                                <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">Ár: </span>
                                Fa: <span class=\"piros\">".$ar["$v[$i]"]["fa"]."</span>
                                Fém: <span class=\"piros\">".$ar["$v[$i]"]["vas"]."</span>
                                <br>
                                <span style=\"font-weight: bold;\">";
                                if( $v[$i] == "kard" ) echo "Kard vásárlás: "; if( $v[$i] == "ij" ) echo "Íj vásárlás: "; if( $v[$i] == "landzsa" ) echo "Lándzsa vásárlás: ";
                                echo "</span>
                                <input type=\"number\" name=\"$v[$i]_vesz\" min=\"0\" max=\"".$szerszam["termeles_"."$v[$i]"]."\" step=\"1\">
                                <br><br>";
                            }
                            ?>
                            <input type="submit" name="egység" value="Küldés" style="float: left">
                            <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'" style="float: left; margin-left: 10px ">

                        </div>
                        </form>

                        <?php
                    }
                    elseif( $epulet == 'laktanya' )
                    {
                        $egyseg = new egyseg_gyartas($_SESSION['id']);
                        $hany = $egyseg->gyarthatoegyseg_laktanya();
                        //print_r($hany);

                        ?>
                        <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet.'&egyseg=1';?>" method="post">
                        <div id="laktanya">
                            <br>
                            <h1 style="font-size: 30px"> Egységek: </h1>

                            <?php
                            $v = array("ijjasz", "kardos", "landzsas" );
                            for( $i=0; $i<3; $i++ )
                            {
                                echo "<span style=\"font-weight: bold; text-shadow: 1px 1px black; font-size: 20px;\">";
                                if( $v[$i] == "ijjasz" ) echo "Íjász: "; if( $v[$i] == "kardos" ) echo "Kardos: "; if( $v[$i] == "landzsas" ) echo "Lándzsás: ";
                                echo "</span><br>
                                Vásárolható: <span class=\"piros\">".$hany[$v[$i]]["epitheto"]."</span> <br>
                                Támadás: <span class=\"piros\">".$hany["$v[$i]"]["tamadas"]."</span>
                                Védelem: <span class=\"piros\">".$hany["$v[$i]"]["vedelem"]."</span>
                                <br>
                                <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">Ár: </span>";
                                if( $hany["$v[$i]"]['koltseg_kard'] !=0 ){ echo "Kard: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_kard"]."  </span>"; }
                                if( $hany["$v[$i]"]['koltseg_ij'] !=0 ){ echo "Íj: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_ij"]."  </span>"; }
                                if( $hany["$v[$i]"]['koltseg_landzsa'] !=0 ){ echo "Lándzsa: "."<span class=\"piros\">".$hany["$v[$i]"]["koltseg_landzsa"]."  </span>"; }
                                if( $hany["$v[$i]"]['buza'] !=0 ){ echo "Búza: "."<span class=\"piros\">".$hany["$v[$i]"]["buza"]." </span>"; }
                                if( $hany["$v[$i]"]['fa'] !=0 ){ echo "Fa: "."<span class=\"piros\">".$hany["$v[$i]"]["fa"]."  </span>"; }
                                if( $hany["$v[$i]"]['teherbiras'] !=0 ){ echo "Teherbírás: "."<span class=\"piros\">".$hany["$v[$i]"]["teherbiras"]."  </span>"; }
                                echo "<br>
                                <span style=\"font-weight: bold; text-shadow: 1px 1px black;\">";
                                if( $v[$i] == "ijjasz" ) echo "Íjász vásárlás: "; if( $v[$i] == "kardos" ) echo "Kardos vásárlás: "; if( $v[$i] == "landzsas" ) echo "Lándzsás vásárlás: ";
                                echo "</span>
                                <input type=\"number\" name=\"$v[$i]_vesz\" min=\"0\" max=\"".$hany["$v[$i]"]["epitheto"]."\" step=\"1\">
                                <br><br>";
                            }
                            ?>
                            <input type="submit" name="egység" value="Küldés" style="float: left">
                            <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'" style="float: left; margin-left: 10px ">
                         </div>
                        </form>
                        <?php
                    }
                ?>
            </div>
        <?php
        } else {
            $nev = "Üres területre építés";

            foreach ($osszEpulet as $kulcs => $ertek) {
                if ($lenneEpulet == $kulcs)
                    $epulet = $ertek[0];
            }

            $ujra = new ujraHasznalando();
            $lenne = $ujra->NevEpulet($epulet);

            $kep = "../resources/images/buildings/" . $epulet . ".png";
            $kovnyers = $jatek->kovnyers($epulet);

            ?>
            <div id="belso">
                <h1><?php echo $nev; ?></h1>
                <img id="bKep" src="<?php echo $kep; ?>">

                <div id="koltseg">
                    <h2>Ide építhető épület: <span class="piros"><?php echo $lenne?></span></h2>
                    <h2>Fejlesztes költségei:</h2>
                    Fa: <span class="piros"><?php print_r($kovnyers['fa_koltseg']) ?></span><br>
                    Kő: <span class="piros"><?php print_r($kovnyers['ko_koltseg']) ?></span><br>
                    Búza: <span class="piros"><?php print_r($kovnyers['buza_koltseg']) ?></span><br>
                    Vas: <span class="piros"><?php print_r($kovnyers['vas_koltseg']) ?></span><br>
                    Minimum feltétel: <span class="piros"><?php print_r($kovnyers['feltetel']) ?></span><br>
                    Fejlesztési idő: <span class="piros"><?php echo $kovnyers['epetesiido'] ?></span><br>
                    <?php
                    if( isset($kovnyers['termeles']) )
                    {
                         echo "Fejlesztéssel való népesség növekedés: <span class=\"piros\">";
                         echo $kovnyers['termeles'];
                         echo "</span><br>";
                    }
                    ?>
                    Következő szint: <span class="piros"><?php print_r($kovnyers['kovetkezoszint']) ?></span><br><br><br>

                </div>

                <form action="<?php echo $_SERVER['PHP_SELF'], '?id=' . $id . '&epul=' . $lenneEpulet;?>"
                      method="post">
                    <input type="submit" value="Fejleszt">
                    <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'">
                </form>
            </div>
        <?php
        }
        ?>
    </div>
</body>
