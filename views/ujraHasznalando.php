<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.04.05.
 * Time: 16:04
 */

class ujraHasznalando
{

    public function NevEpulet($epulet)
    {
        if ($epulet == "var")
            return "Vár";
        else if ($epulet == "favago")
            return "Favágó";
        else if ($epulet == "kobanya")
            return "Kőbánya";
        else if ($epulet == "farm")
            return "Farm";
        else if ($epulet == "ercbanya")
            return "Ércbánya";
        else if ($epulet == "lakohaz")
            return "Lakóház";
        else if ($epulet == "kovacs")
            return "Kovács";
        else if ($epulet == "laktanya")
            return "Laktanya";
        else if ($epulet == "piac")
            return "Piac";
        else if ($epulet == "kocsma")
            return "Kocsma";
        else if ($epulet == "fal")
            return "Fall";
        else if ($epulet == "katedralis")
            return "Katedrális";
        else if ($epulet == "kutato")
            return "Kutaó";
        else if ($epulet == "ortorony")
            return "Őrtorony";

    }

    public function OsszEpulet()
    {
        $osszEpulet[0] = array("var", "1154px", "738px" );
        $osszEpulet[1] = array("favago", "1072px", "1068px" );
        $osszEpulet[2] = array("kobanya", "1486px", "409px" );
        $osszEpulet[3] = array("farm", "579px", "903px" );
        $osszEpulet[4] = array("ercbanya", "1401px", "409px" );
        $osszEpulet[5] = array("lakohaz", "1320px", "656px" );
        $osszEpulet[6] = array("kovacs", "825px", "490px" );
        $osszEpulet[7] = array("laktanya", "662px", "1068px" );
        $osszEpulet[8] = array("piac", "907px", "903px" );
        $osszEpulet[9] = array("kocsma", "1154px", "574px" );
        $osszEpulet[10] = array("fal", "742px", "1233px" );
        $osszEpulet[11] = array("katedralis", "1649px", "656px" );
        $osszEpulet[12] = array("kutato", "990px", "325px" );
        $osszEpulet[13] = array("ortorony", "579px", "1233px" );

        return $osszEpulet;
    }
}

?>