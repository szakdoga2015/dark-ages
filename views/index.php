<!--<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.02.17.
 * Time: 9:06
 */
/*header("Content-Type: text/html; charset=UTF-8");
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
require_once($_SERVER["DOCUMENT_ROOT"]."/dark-ages/controls/mailer.php");
if(empty($_GET)) {
    $mail = new Mailer();
    $mail->activation("csuka15@gmail.com", "zoli");
}else{
    echo $_GET['activation'];
}*/
?>
-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="index.css">

    <script type="text/javascript" charset="utf-8" src="http://code.jquery.com/jquery-latest.min.js"></script>
	
</head>
<body onload='setFocusToTextBox()'>
<?php
/**
 * aktiválást ellenőriz
 */
    require_once '../controls/mysqlkapcs.php';
if(!empty($_GET['activation'])){
   $mysql=new mysqlkapcs();

   $mysql->aktivalas($_GET['activation']);

}
?>
<div id="menu">
    <a href="bejelentkezes">Játssz!</a>
    <a href="hirek">Hírek!</a>
    <a href="kontakt">Kontakt</a>
    <a href="regisztracio">Regisztráció</a>
</div>

<img src="../resources/images/logo.jpg" alt="Dark Ages" style="width:100%">

<section id="content">

    <div id="hirek">
        <div id="fejlec">
            Hírek
        </div>
        <div id="newhir">
            <h1 class="hhir">Fejlesztés</h1>
                    <p class="szhir">
                        A Dark Ages gőzerővel fejlődik napról napra, hogy egy kellemes játék jöjjön létre.
                        Teszteljünk is kicsit:
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget vehicula eros. Praesent varius elementum ullamcorper. Vestibulum sed facilisis massa.
                        Nullam posuere ultricies ullamcorper. Pellentesque sagittis ante eu nisl tempus mollis a ut ipsum.
                        Aliquam facilisis egestas magna, nec ullamcorper sapien consectetur vitae. Sed fermentum egestas fringilla. Etiam eleifend sollicitudin felis ut facilisis.
                    </p>
        </div>
        <hr>
        <div id="newhir">
            <h1 class="hhir">Itt hírek lesznek</h1>
            <p class="szhir">
                A Dark Ages fejlődik.
                Teszteljünk is kicsit:
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget vehicula eros. Praesent varius elementum ullamcorper. Vestibulum sed facilisis massa.
                Nullam posuere ultricies ullamcorper. Pellentesque sagittis ante eu nisl tempus mollis a ut ipsum.
                Aliquam facilisis egestas magna, nec ullamcorper sapien consectetur vitae. Sed fermentum egestas fringilla. Etiam eleifend sollicitudin felis ut facilisis.
            </p>
        </div>
        <br><br>
    </div>
</section>
<br><br><br>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){

        $('#menu a').click(function(){
            $('#content').load('pages.php #' + $(this).attr('href'))
            document.getElementById("fejlec").scrollIntoView()
			var s = document.createElement("script");
			s.type = "text/javascript";
			s.src = "https://www.google.com/recaptcha/api.js";
            $("head").append(s);
            return false;
        });

    });
	
	function setFocusToTextBox(){}
</script>

</body>
</html>