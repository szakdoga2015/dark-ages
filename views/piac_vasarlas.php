<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.05.05.
 * Time: 13:02
 */

session_start();
require_once "../controls/kereskedes.php";
require_once "../views/ujraHasznalando.php";
$kereskedes = new kereskedes($_SESSION['id']);
$ujra = new ujraHasznalando();
//$kereskedes->kereskedesveg();

include "../views/tarolando.php";
print_r($nyilt);
if(  $nyilt == "" )
    $nyilt = $kereskedes->nyiltajanlat();

foreach( $nyilt as $key=>$value )
{
    $split=explode("_",$key);
    if( isset($split[1]) )
    {
        if( $split[1] == "ajanl" && $value>0 )
        {
            $nyilt_mit = $split[0];
            $nyilt_mit_mennyi = $value;
        }
        else if( $split[1] == "keres" && $value>0 )
        {
            $nyilt_mire = $split[0];
            $nyilt_mire_mennyi = $value;
        }
    }

}

$id = $_GET['id'];
$lenneEpulet = $_GET['epul'];

?>
<head>
    <style>
        #pergament{
            height: 700px;
        }
    </style>
</head>

<form action="<?php echo '../views/epuletmodosul.php?id=' . $id . '&epul=' . $lenneEpulet.'&keresked';?>" method="post">

    <div id="nyilt_kereskedo_felulet">
        Vásárlás: Mit - Miért
        <select name="nyilt" size="10" style="width: 100%">
            <option value="" selected></option>
            <?php
            foreach( $nyilt as $key =>$value )
            {
                foreach( $value as $kulcs=>$ertek )
                {
                    if( $kulcs = "id" )
                        $jatekos = $ertek;

                    if( strpos($kulcs,'_') !== false )
                    {
                        $plit = explode("_", $kulcs);

                        if( $ertek !=0 && $plit[1] == 'ajanl' )
                        {
                            $ajanlN = $plit[0];
                            $ajanlE = $ertek;
                        }

                        if( $ertek !=0 && $plit[1] == 'keres' )
                        {
                            $keresN = $plit[0];
                            $keresE = $ertek;
                        }
                    }
                }
                $nevek = array( 'buza'=>'Búza', 'fa'=>'Fa', 'vas'=>'Vas', 'ko'=>'Kő' );

                foreach( $nevek as $key=>$value )
                {
                    if( $key == $ajanlN )
                        $ajanlN = $value;

                    if( $key == $keresN )
                        $keresN = $value;
                }

                echo "<option value=\"$jatekos\">$ajanlN: $ajanlE - $keresN: $keresE</option>";
            }

            ?>
        </select>
    </div>
    <div id="kereskedo_felulet">
        <br>
        <span class="piac_cimkék">Kínál:</span>
        <select name="Kínál" size="1">
            <option value=""></option>
            <option value="buza">Búza</option>
            <option value="fa">Fa</option>
            <option value="vas">Vas</option>
            <option value="ko">Kő</option>
        </select>
        <br>
        <span class="piac_cimkék">Keres:</span>
        <select name="Keres" size="1">
            <option value=""></option>
            <option value="buza">Búza</option>
            <option value="fa">Fa</option>
            <option value="vas">Vas</option>
            <option value="ko">Kő</option>
        </select>
        <br><br>
        <span class="piac_cimkék">Elad:</span>
        <br>
        Mit:
        <select name="Mit" size="1">
            <option value=""></option>
            <option value="buza">Búza</option>
            <option value="fa">Fa</option>
            <option value="vas">Vas</option>
            <option value="ko">Kő</option>
        </select>
        Mennyiség: <input type="number" name="mit_mennyi" min="0" value="0" style="width: 25%;">
        <br>
        Mire:
        <select name="Mire" size="1">
            <option value=""></option>
            <option value="buza">Búza</option>
            <option value="fa">Fa</option>
            <option value="vas">Vas</option>
            <option value="ko">Kő</option>
        </select>
        Mennyiség: <input type="number" name="mire_mennyi" min="0" value="0" style="width: 25%;">
    </div>
    <div id="piac_kuldes">
        <input type="submit" name="keresked" value="Kereskedés" style="float: left; margin-top: 5%">
        <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'" style="float: left; margin-left: 10px; margin-top: 5% ">
    </div>
</form>
