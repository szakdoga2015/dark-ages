<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.05.05.
 * Time: 12:35
 */

foreach ($osszEpulet as $kulcs => $ertek) {
    if ($id == $kulcs)
        $epulet = $ertek[0];
}

$kep = "../resources/images/buildings/" . $epulet . ".png";

$nev = $ujra->NevEpulet($epulet);
$nev .= " fejlesztése";

$kovnyers = $jatek->kovnyers($epulet);

?>

<h1><?php echo $nev; ?></h1>
<img id="bKep" src="<?php echo $kep; ?>">

<div id="koltseg">
    <h2>Fejlesztes költségei:</h2>
    Fa: <span class="piros"><?php print_r($kovnyers['fa_koltseg']) ?></span><br>
    Kő: <span class="piros"><?php print_r($kovnyers['ko_koltseg']) ?></span><br>
    Búza: <span class="piros"><?php print_r($kovnyers['buza_koltseg']) ?></span><br>
    Vas: <span class="piros"><?php print_r($kovnyers['vas_koltseg']) ?></span><br>
    Minimum feltétel: <span class="piros"><?php print_r($kovnyers['feltetel']) ?></span><br>
    Fejlesztési idő: <span class="piros"><?php print_r($kovnyers['epetesiido']) ?></span><br>
    <?php
    if( isset($kovnyers['termeles']) )
    {
        echo "Fejlesztéssel való népesség növekedés: <span class=\"piros\">";
        echo $kovnyers['termeles'];
        echo "</span><br>";
    }
    ?>
    Következő szint: <span class="piros"><?php print_r($kovnyers['kovetkezoszint']) ?></span><br><br>
</div>