<?php
/**
 * Created by PhpStorm.
 * User: Zoli
 * Date: 2015.03.18.
 * Time: 16:16
 */
//session_start();
//echo $_SESSION['nev'];
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="15" >
    <title>Dark Ages</title>

    <style>
        html {
            background-color: #3c7d32;
        }

        #adatok{
            position: absolute;
            text-align: left;
            color: white;
            width: 370px;
            height: 600px;
            display:block;
            left:80px;
            top:70px;
            margin: 0;
            padding: 0;
        }

        #adatok p {
            font-size: 20px;
        }

        #adatok h1 {
            font-size: 27px;
        }
    </style>

    <script>

        $( document ).ready(function() {
            var alapw = 1920, alaph = 1080;

            var aranyW = window.outerWidth/alapw;
            var aranyH = window.outerHeight/alaph;

            $( "#map").css( "width", parseInt($( "#map").css( "width")) * aranyW);

            $("#adatok").css("left", parseInt($("#adatok").css("left"))*aranyW);
            $("#adatok").css("top", parseInt($("#adatok").css("top"))*aranyW);

            $("#adatok p").css("fontSize", parseInt($("#adatok p").css("fontSize"))*aranyW);
            $("#adatok h1").css("fontSize", parseInt($("#adatok h1").css("fontSize"))*aranyW);


            $( ".buildings").each(function(){
                var cssValue={
                    width: parseInt($(this).css( "width")) * aranyW,
                    height: parseInt($(this).css( "height")) * aranyW,
                    left: parseInt($(this).css( "left")) * aranyW,
                    top: parseInt($(this).css( "top")) * aranyW
                };
                $(this).css(cssValue);
            });
        });

        function epuletModosul( b, c ) {
            location.href="../views/epuletmodosul.php?id=" + b + "&epul=" + c;
        }

    </script>

</head>
<body>

<div style="position: relative; left: 0; top: 0; ">
    <img id="map" src="../resources/images/map.png" alt="Dark Ages" style="width: 1887px; position: relative; top: 0; left: 0;">

    <?php
    session_start();

    require_once "../views/ujraHasznalando.php";
    require_once "../controls/jatek.php";
    require_once "../controls/egyseg_gyartas.php";
    require_once "../controls/tamadascontrol.php";

    $jatek = new jatek( $_SESSION['id'] );
    $ujra = new ujraHasznalando();
    $tamad = new tamadascontrol($_SESSION['id']);

    $tamad->utazasvege();$tamad->utazasvege();
    $epuletek = $jatek->getepuletek();
    $jatek->frisitnyers();
    $jatek->epuletkesz();
    $osszEpulet = $ujra -> OsszEpulet();

    foreach($epuletek as $key=>$value)
    {
        foreach( $osszEpulet as $kulcs=>$ertek )
        {
            if( $key == $ertek[0] )
            {
                if( $value == 0 )
                    echo "<img class=\"buildings\" name=\"$ertek[0]\" src=\"../resources/images/buildings/sand.png\" width=\"82\" height=\"82\" onclick=\"epuletModosul(-1,$kulcs)\"  style=\" position: absolute; left: $ertek[1]; top: $ertek[2];\">";
                else
                    echo "<img class=\"buildings\" name=\"$ertek[0]\" src=\"../resources/images/buildings/$ertek[0].png\" width=\"82\" height=\"82\" onclick=\"epuletModosul($kulcs,0)\"  style=\" position: absolute; left: $ertek[1]; top: $ertek[2];\">";
            }
        }
    }

    $nyers = $jatek->getnyersanyag();
    $egyseg = new egyseg_gyartas($_SESSION['id']);
    $egysegek = $egyseg->egysegek();

    ?>
        <img id="adat" src="../resources/images/map_adatok.png" style="position:absolute; left: 0; top: 0; width: 20%; height: 36%;">
    <div id="adatok">
        <h1>Üdvözöllek <?php echo $_SESSION["nev"] ?>!</h1>
        <p>
            Fa: <?php print_r($nyers['fa']) ?><br>
            Kő: <?php print_r($nyers['ko']) ?><br>
            Vas: <?php print_r($nyers['vas']) ?><br>
            Búza: <?php print_r($nyers['buza']) ?><br><br>
            Kardos: <?php print_r($egysegek['kardos']) ?><br>
            Íjász: <?php print_r($egysegek['ijjasz']) ?><br>
            Lándzsás: <?php print_r($egysegek['landzsas']) ?><br>
            Kereskedő: <?php print_r($egysegek['kereskedo']) ?><br>
            Szekér: <?php print_r($egysegek['szeker']) ?><br><br>
            Kard: <?php print_r($nyers['kard']) ?><br>
            Íj: <?php print_r($nyers['ij']) ?><br>
            Lándzsa: <?php print_r($nyers['landzsa']) ?><br>

        </p>
        <input type="Button" value="Kilépés" onclick="<?php echo session_write_close() ?>$:location.href='../views/index.php'">
    </div>
</div>
</body>
</html>

