<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.05.05.
 * Time: 13:05
 */


header('Content-Type: text/html; charset=utf-8');

session_start();
require_once "../views/ujraHasznalando.php";
require_once "../controls/jatek.php";

$jatek = new jatek( $_SESSION['id'] );
$ujra = new ujraHasznalando();

$osszEpulet = $ujra -> OsszEpulet();

$jatek->frisitnyers();
$jatek->epuletkesz();

$id = $_GET['id'];
$lenneEpulet = $_GET['epul'];

if( isset($_POST['fejlesz']) ) {

    if ($id != -1) {

        foreach ($osszEpulet as $kulcs => $ertek) {
            if ($id == $kulcs)
                $epulet = $ertek[0];
        }
    } else {
        foreach ($osszEpulet as $kulcs => $ertek) {
            if ($lenneEpulet == $kulcs)
                $epulet = $ertek[0];
        }
    }
    $üzi = $jatek->kovszintepit($epulet);

    if (empty($üzi)) {
        echo "<script type='text/javascript'>alert('Sikeres fejlesztés!');</script>";
    } else {
        echo "<script type='text/javascript'>alert('$üzi');</script>";
    }
}

include 'epulet_adat.php';
?>
<form action="<?php echo '../views/epuletmodosul.php?id=' . $id . '&epul=' . $lenneEpulet ;?>"  method="post" style="padding-bottom: 10%;">
    <input type="submit" name="fejlesz1" value="Fejleszt">
    <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'">
</form>