<?php
/**
 * Created by PhpStorm.
 * User: SarahDreamfire
 * Date: 2015.05.06.
 * Time: 16:01
 */

session_start();
require_once "../controls/tamadascontrol.php";
require_once "../controls/egyseg_gyartas.php";

$tamadas = new tamadascontrol($_SESSION['id']);
$tamadhatok = $tamadas->tamadhato_emberek();

$egyseg = new egyseg_gyartas($_SESSION['id']);
$hany = $egyseg->egysegek();

$id = $_GET['id'];
$lenneEpulet = $_GET['epul'];
?>

<head>
    <style>
        #pergament{
            height: 700px;
        }
    </style>
</head>

<form action="<?php echo '../views/epuletmodosul.php?id=' . $id . '&epul=' . $lenneEpulet;?>" method="post">
    <div id="laktanya_tamad">
            <h2 style="shadow: 2px 2px black;">Támadható játékosok :<h2>
            <select name="tamad" size="10" style="width: 100%">
                <option value="" selected></option>
                <?php
                foreach( $tamadhatok as $key =>$value )
                {
                    foreach( $value as $kulcs=>$ertek )
                    {
                        if( $kulcs == 'id')
                            $jatekos = $ertek;

                        if( $kulcs == 'nev' )
                            $nev = $ertek;

                        if( $kulcs == "tamadasiido" )
                            $ido = $ertek;
                    }

                    echo "<option value=\"$jatekos\">Neve : $nev   Támadási idő : $ido </option>";
                }

                ?>
            </select>

        <h2 style="shadow: 2px 2px black;">Támadásra Küldött egységek:</h2>

        <span style="color: red">Kardos: </span><input type=number name=kardos_kuld" min="0" max="<?php echo $hany['kardos']?>" step="1"><br>
        <span style="color: red">Íjász: </span><input type=number name="ijasz_kuld" min="0" max="<?php echo $hany['ijjasz']?>" step="1"><br>
        <span style="color: red">Lándzsás: </span><input type=number name="landzsas_kuld" min="0" max="<?php echo $hany['landzsas']?>" step="1"><br>
        <span style="color: red">Szekér: </span><input type=number name="szeker_kuld" min="0" max="<?php echo $hany['szeker']?>" step="1"><br><br>

        <input type="submit" name="Ltamadas" value="Küldés" style="float: left">
        <input type="Button" value="Vissza" onclick="$:location.href='../views/jatek.php'" style="float: left; margin-left: 10px ">
    </div>
</form>