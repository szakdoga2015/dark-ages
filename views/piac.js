/**
 * Created by SarahDreamfire on 2015.05.05..
 */

$(document).ready(function () {

    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }

    /*if( isset($_GET['keresked']) ) {
        alert('yes');
        $('#content').load('../views/piac_vasarlas.php?id=8&epul=0');
    }
    else*/
        $('#content').load('../views/ajax_adat.php?id=8&epul=0');

    $('ul#nav li a').click(function(){
        var page = $(this).attr('href');
        $('#content').load('../views/' + page );
        return false;
    });
});
